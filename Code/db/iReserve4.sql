--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.0
-- Dumped by pg_dump version 9.5.0

-- Started on 2016-05-25 12:39:38

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 202 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2302 (class 0 OID 0)
-- Dependencies: 202
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 254 (class 1255 OID 52754)
-- Name: confirm_transaction(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION confirm_transaction(par_payment_id character varying) RETURNS void
    LANGUAGE sql
    AS $$
  UPDATE transaction
SET
  confirmed = true
WHERE payment_id = par_payment_id;
$$;


ALTER FUNCTION public.confirm_transaction(par_payment_id character varying) OWNER TO postgres;

--
-- TOC entry 203 (class 1255 OID 52755)
-- Name: deactivate(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION deactivate(par_id_personnel integer) RETURNS void
    LANGUAGE sql
    AS $$
       UPDATE Hotel_Personnel
       SET
         is_active = FALSE

       WHERE id_personnel = par_id_personnel;
$$;


ALTER FUNCTION public.deactivate(par_id_personnel integer) OWNER TO postgres;

--
-- TOC entry 204 (class 1255 OID 52756)
-- Name: deactivate_featurelist(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION deactivate_featurelist(par_id_hotel_features_list integer) RETURNS void
    LANGUAGE sql
    AS $$
   UPDATE Hotel_features_list
   SET
     is_active = FALSE

   WHERE id_hotel_features_list = par_id_hotel_features_list;
$$;


ALTER FUNCTION public.deactivate_featurelist(par_id_hotel_features_list integer) OWNER TO postgres;

--
-- TOC entry 205 (class 1255 OID 52757)
-- Name: deactivate_hotel(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION deactivate_hotel(par_id integer) RETURNS void
    LANGUAGE sql
    AS $$
       UPDATE Hotel
       SET
         is_active = FALSE

       WHERE id_hotel = par_id;
$$;


ALTER FUNCTION public.deactivate_hotel(par_id integer) OWNER TO postgres;

--
-- TOC entry 206 (class 1255 OID 52758)
-- Name: deactivate_hotelfeature(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION deactivate_hotelfeature(par_id_hotel_features integer) RETURNS void
    LANGUAGE sql
    AS $$
   UPDATE Hotel_features
   SET
     is_active = FALSE

   WHERE id_hotel_features = par_id_hotel_features;
$$;


ALTER FUNCTION public.deactivate_hotelfeature(par_id_hotel_features integer) OWNER TO postgres;

--
-- TOC entry 207 (class 1255 OID 52759)
-- Name: deletefeedback(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION deletefeedback(par_feedback_id integer) RETURNS void
    LANGUAGE sql
    AS $$
  UPDATE feedback
  SET is_active = False
  WHERE feedback_id = par_feedback_id;
$$;


ALTER FUNCTION public.deletefeedback(par_feedback_id integer) OWNER TO postgres;

--
-- TOC entry 208 (class 1255 OID 52760)
-- Name: done_transaction(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION done_transaction(par_payment_id character varying) RETURNS void
    LANGUAGE sql
    AS $$
UPDATE transaction
SET
  is_done = TRUE
WHERE payment_id = par_payment_id;
$$;


ALTER FUNCTION public.done_transaction(par_payment_id character varying) OWNER TO postgres;

--
-- TOC entry 209 (class 1255 OID 52761)
-- Name: get_mostvisited(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_mostvisited() RETURNS SETOF record
    LANGUAGE sql
    AS $$

    SELECT count, hotel_id from visited ORDER BY count

  $$;


ALTER FUNCTION public.get_mostvisited() OWNER TO postgres;

--
-- TOC entry 222 (class 1255 OID 52762)
-- Name: getadmin_id(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getadmin_id(par_id_admin integer, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT boolean) RETURNS SETOF record
    LANGUAGE sql
    AS $$

	SELECT fname, lname, email_address, password, is_system_admin
		FROM System_admin WHERE id_admin = par_id_admin;

$$;


ALTER FUNCTION public.getadmin_id(par_id_admin integer, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT boolean) OWNER TO postgres;

--
-- TOC entry 223 (class 1255 OID 52763)
-- Name: getbusinessinfo(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getbusinessinfo(OUT integer, OUT character varying, OUT character varying, OUT character varying, OUT integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$

      SELECT id_info, business_license, tax_identification_number, proof_picture, hotel_id FROM business_information

  $$;


ALTER FUNCTION public.getbusinessinfo(OUT integer, OUT character varying, OUT character varying, OUT character varying, OUT integer) OWNER TO postgres;

--
-- TOC entry 224 (class 1255 OID 52764)
-- Name: getbusinessinfo_id(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getbusinessinfo_id(par_id_info integer, OUT character varying, OUT character varying, OUT character varying, OUT integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$

      SELECT business_license, tax_identification_number, proof_picture, hotel_id FROM business_information WHERE id_info = par_id_info;

  $$;


ALTER FUNCTION public.getbusinessinfo_id(par_id_info integer, OUT character varying, OUT character varying, OUT character varying, OUT integer) OWNER TO postgres;

--
-- TOC entry 225 (class 1255 OID 52765)
-- Name: getfeatures_list(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getfeatures_list(OUT integer, OUT character varying, OUT integer, OUT boolean) RETURNS SETOF record
    LANGUAGE sql
    AS $$
    select id_hotel_features_list, name, hotel_features, is_active from Hotel_features_list;
$$;


ALTER FUNCTION public.getfeatures_list(OUT integer, OUT character varying, OUT integer, OUT boolean) OWNER TO postgres;

--
-- TOC entry 226 (class 1255 OID 52766)
-- Name: getfeedback(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getfeedback(OUT integer, OUT text, OUT text, OUT timestamp without time zone, OUT boolean, par_hotel_id integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$
  SELECT feedback_id, name, comment, created_date, is_active
  FROM feedback
  WHERE hotel_id = par_hotel_id
  AND is_active = TRUE  ;
$$;


ALTER FUNCTION public.getfeedback(OUT integer, OUT text, OUT text, OUT timestamp without time zone, OUT boolean, par_hotel_id integer) OWNER TO postgres;

--
-- TOC entry 227 (class 1255 OID 52767)
-- Name: getfeedback_id(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getfeedback_id(par_feedback_id integer, OUT text, OUT text, OUT timestamp without time zone, OUT boolean, OUT integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$
  SELECT name, comment, created_date, is_active, hotel_id FROM Feedback WHERE feedback_id = par_feedback_id;
$$;


ALTER FUNCTION public.getfeedback_id(par_feedback_id integer, OUT text, OUT text, OUT timestamp without time zone, OUT boolean, OUT integer) OWNER TO postgres;

--
-- TOC entry 228 (class 1255 OID 52768)
-- Name: gethotel(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION gethotel(OUT integer, OUT character varying, OUT character varying, OUT text, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT integer, OUT text, OUT boolean, OUT boolean, OUT boolean) RETURNS SETOF record
    LANGUAGE sql
    AS $$

  SELECT id_hotel, hotel_name, website_url, description, email_address, paypal_account, password, address, contact_number, map_latitude,
      map_longitude, no_of_rooms, extra, is_verified, is_hotel_admin, is_active FROM Hotel;

$$;


ALTER FUNCTION public.gethotel(OUT integer, OUT character varying, OUT character varying, OUT text, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT integer, OUT text, OUT boolean, OUT boolean, OUT boolean) OWNER TO postgres;

--
-- TOC entry 229 (class 1255 OID 52769)
-- Name: gethotel_features(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION gethotel_features(OUT integer, OUT character varying, OUT integer, OUT boolean) RETURNS SETOF record
    LANGUAGE sql
    AS $$
    select id_hotel_features, name, hotel_id, is_active from Hotel_features;
$$;


ALTER FUNCTION public.gethotel_features(OUT integer, OUT character varying, OUT integer, OUT boolean) OWNER TO postgres;

--
-- TOC entry 261 (class 1255 OID 53410)
-- Name: gethotel_id(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION gethotel_id(par_id integer, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT integer, OUT text, OUT boolean, OUT boolean, OUT boolean) RETURNS SETOF record
    LANGUAGE sql
    AS $$
  SELECT 
    hotel.email_address,
    hotel.hotel_name,
    hotel.website_url,
    hotel.description,
    hotel.paypal_account,
    hotel.address,
    hotel.contact_number,
    hotel.no_of_rooms,
    hotel.extra,
    hotel.is_verified,
    hotel.is_hotel_admin,
    hotel.is_active

  FROM Hotel
    
  WHERE id_hotel = par_id;
$$;


ALTER FUNCTION public.gethotel_id(par_id integer, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT integer, OUT text, OUT boolean, OUT boolean, OUT boolean) OWNER TO postgres;

--
-- TOC entry 230 (class 1255 OID 52771)
-- Name: gethotel_personnel(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION gethotel_personnel(OUT integer, OUT text, OUT text, OUT text, OUT text, OUT text, OUT boolean, OUT boolean, OUT integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$

  SELECT id_personnel, fname, mname, lname, email, personnel_password, is_active, is_personnel, hotel_id FROM Hotel_Personnel;

$$;


ALTER FUNCTION public.gethotel_personnel(OUT integer, OUT text, OUT text, OUT text, OUT text, OUT text, OUT boolean, OUT boolean, OUT integer) OWNER TO postgres;

--
-- TOC entry 270 (class 1255 OID 61609)
-- Name: getid_features(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getid_features(par_id_hotel integer, OUT integer, OUT character varying) RETURNS SETOF record
    LANGUAGE sql
    AS $$
  SELECT 
    hotel_features.id_hotel_features,
    hotel_features.name                     
 FROM hotel_features                     
 WHERE hotel_features.hotel_id = par_id_hotel;
$$;


ALTER FUNCTION public.getid_features(par_id_hotel integer, OUT integer, OUT character varying) OWNER TO postgres;

--
-- TOC entry 264 (class 1255 OID 61611)
-- Name: getid_featureslist(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getid_featureslist(par_id_hotel_features integer, OUT integer, OUT character varying) RETURNS SETOF record
    LANGUAGE sql
    AS $$
  select id_hotel_features_list, name from Hotel_features_list where hotel_features = par_id_hotel_features;
$$;


ALTER FUNCTION public.getid_featureslist(par_id_hotel_features integer, OUT integer, OUT character varying) OWNER TO postgres;

--
-- TOC entry 262 (class 1255 OID 53415)
-- Name: getid_personnel(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getid_personnel(par_hotel_id integer, OUT integer, OUT text, OUT text, OUT text, OUT text, OUT boolean, OUT boolean) RETURNS SETOF record
    LANGUAGE sql
    AS $$
  SELECT id_personnel, fname, mname, lname, email, is_active, is_personnel
     FROM Hotel_Personnel WHERE hotel_id = par_hotel_id;
$$;


ALTER FUNCTION public.getid_personnel(par_hotel_id integer, OUT integer, OUT text, OUT text, OUT text, OUT text, OUT boolean, OUT boolean) OWNER TO postgres;

--
-- TOC entry 268 (class 1255 OID 61839)
-- Name: getid_room(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getid_room(par_hotel_id integer, OUT character varying, OUT integer, OUT integer, OUT integer, OUT integer, OUT integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$
  SELECT                           
    room.room_name,                  
    room.cost,                       
    room.max_person,                 
    room.available_room,             
    room.total_room,
    room.id_room
  FROM room                                           
  WHERE room.hotel_id = par_hotel_id;
$$;


ALTER FUNCTION public.getid_room(par_hotel_id integer, OUT character varying, OUT integer, OUT integer, OUT integer, OUT integer, OUT integer) OWNER TO postgres;

--
-- TOC entry 231 (class 1255 OID 52776)
-- Name: getimage(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getimage(OUT integer, OUT character varying, OUT integer, OUT integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$

    SELECT id_image, img, hotel_id, room_id FROM Image;

  $$;


ALTER FUNCTION public.getimage(OUT integer, OUT character varying, OUT integer, OUT integer) OWNER TO postgres;

--
-- TOC entry 255 (class 1255 OID 61838)
-- Name: getimage_id(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getimage_id(par_hotel_id integer, OUT integer, OUT character varying) RETURNS SETOF record
    LANGUAGE sql
    AS $$
  SELECT id_image, img FROM Image WHERE hotel_id = par_hotel_id;
$$;


ALTER FUNCTION public.getimage_id(par_hotel_id integer, OUT integer, OUT character varying) OWNER TO postgres;

--
-- TOC entry 253 (class 1255 OID 53395)
-- Name: getpassword(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getpassword(par_email text) RETURNS SETOF record
    LANGUAGE sql
    AS $$
  select password from hotel system_admin where email_address = par_email;
$$;


ALTER FUNCTION public.getpassword(par_email text) OWNER TO postgres;

--
-- TOC entry 232 (class 1255 OID 52779)
-- Name: getroom(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getroom(OUT integer, OUT character varying, OUT integer, OUT integer, OUT integer, OUT integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$

  SELECT id_room, room_type, cost, max_person, total_room, hotel_id FROM Room;

$$;


ALTER FUNCTION public.getroom(OUT integer, OUT character varying, OUT integer, OUT integer, OUT integer, OUT integer) OWNER TO postgres;

--
-- TOC entry 233 (class 1255 OID 52780)
-- Name: getsystemadmin(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION getsystemadmin(OUT integer, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT boolean) RETURNS SETOF record
    LANGUAGE sql
    AS $$
	SELECT id_admin, fname, lname, email_address, password, is_system_admin FROM System_admin;

$$;


ALTER FUNCTION public.getsystemadmin(OUT integer, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT boolean) OWNER TO postgres;

--
-- TOC entry 234 (class 1255 OID 52781)
-- Name: gettransaction(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION gettransaction(par_hotel_id integer, OUT integer, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT integer, OUT character varying, OUT date, OUT date, OUT integer, OUT integer, OUT boolean, OUT boolean) RETURNS SETOF record
    LANGUAGE sql
    AS $$
SELECT
  id_transaction,
  transaction_number,
  payer_id,
  paypal_token,
  payment_id,
  name,
  contact_number,
  email_address,
  checkin_date,
  checkout_date,
  room_id,
  fee,
  confirmed,
  is_done
FROM Transaction
WHERE hotel_id = par_hotel_id;
$$;


ALTER FUNCTION public.gettransaction(par_hotel_id integer, OUT integer, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT integer, OUT character varying, OUT date, OUT date, OUT integer, OUT integer, OUT boolean, OUT boolean) OWNER TO postgres;

--
-- TOC entry 269 (class 1255 OID 61842)
-- Name: location_search(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION location_search(par_search character varying, OUT integer, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT integer, OUT text, OUT integer, OUT character varying, OUT integer, OUT integer, OUT integer, OUT integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$
  SELECT
  hotel.id_hotel,
  hotel.hotel_name,
  hotel.website_url,
  hotel.description,
  hotel.email_address,
  hotel.paypal_account,
  hotel.address,
  hotel.contact_number,
  hotel.no_of_rooms,
  hotel.extra,
  room.id_room,
  room.room_name,
  room.cost,
  room.max_person,
  room.available_room,
  room.total_room

FROM hotel
  INNER JOIN room ON hotel.id_hotel = room.hotel_id
WHERE lower(address) LIKE '%' || lower(par_search) || '%'
      OR lower(address) LIKE lower(par_search) || '%'
      OR lower(address) LIKE '%' || lower(par_search)
      AND hotel.is_active = TRUE;
$$;


ALTER FUNCTION public.location_search(par_search character varying, OUT integer, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT character varying, OUT integer, OUT text, OUT integer, OUT character varying, OUT integer, OUT integer, OUT integer, OUT integer) OWNER TO postgres;

--
-- TOC entry 235 (class 1255 OID 52783)
-- Name: login_system_admin(character varying, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION login_system_admin(par_admin_email_add character varying, par_admin_password text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    loc_email_add text;
    loc_admin_password text;
    loc_res text;
  BEGIN
    select into loc_email_add par_admin_email_add, loc_admin_password par_admin_password from System_admin where email_address = par_email_email_add and password = par_admin_password;

    if loc_email_add isnull or loc_admin_password isnull or loc_email_add = '' or loc_admin_password = '' then
      loc_res = 'Invalid Email or Password';
    else
      loc_res = 'Successfully Logged In';
    end if;
    return loc_res;

  END;
$$;


ALTER FUNCTION public.login_system_admin(par_admin_email_add character varying, par_admin_password text) OWNER TO postgres;

--
-- TOC entry 241 (class 1255 OID 52784)
-- Name: loginauth(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION loginauth(par_email text, par_personnel_password text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    loc_id int;
    loc_res text;
  BEGIN
    select into loc_id id_hotel from hotel where email_address = par_email and password = par_personnel_password;

    if loc_id isnull then
      loc_res = 'ERROR';
    else
      loc_res = loc_id;
    end if;
    return loc_res;

  END;
$$;


ALTER FUNCTION public.loginauth(par_email text, par_personnel_password text) OWNER TO postgres;

--
-- TOC entry 265 (class 1255 OID 61612)
-- Name: loginpersonnelauth(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION loginpersonnelauth(par_email text, par_personnel_password text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    loc_id int;
    loc_res text;
  BEGIN
    select into loc_id hotel_id from hotel_personnel where email = par_email and personnel_password = par_personnel_password;

    if loc_id isnull then
      loc_res = 'ERROR';
    else
      loc_res = loc_id;
    end if;
    return loc_res;

  END;
$$;


ALTER FUNCTION public.loginpersonnelauth(par_email text, par_personnel_password text) OWNER TO postgres;

--
-- TOC entry 236 (class 1255 OID 52785)
-- Name: newbusinessinfo(character varying, character varying, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION newbusinessinfo(par_license character varying, par_taxnum character varying, par_proof_pic character varying, par_hotel_id integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    loc_license Varchar;
    loc_res text;
    BEGIN
      SELECT INTO loc_license business_license FROM business_information WHERE business_license = par_license;
      if loc_license isnull THEN

      INSERT INTO business_information(business_license,tax_identification_number,proof_picture, hotel_id)
            VALUES(par_license, par_taxnum, par_proof_pic,par_hotel_id);
            loc_res = 'OK';

      ELSE
        loc_res = 'ID EXISTED';

      end if;
        return loc_res;
      END;
  $$;


ALTER FUNCTION public.newbusinessinfo(par_license character varying, par_taxnum character varying, par_proof_pic character varying, par_hotel_id integer) OWNER TO postgres;

--
-- TOC entry 237 (class 1255 OID 52786)
-- Name: newfeature_list(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION newfeature_list(par_name character varying, par_hotel_features integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
    DECLARE
        loc_name TEXT;
        loc_res TEXT;
    BEGIN
       select into loc_name name from Hotel_features_list where name = par_name;
            if loc_name isnull THEN

          if par_name = '' THEN
          loc_res = 'Error';
        else
          insert into Hotel_features_list(name, hotel_features) values (par_name, par_hotel_features);
              loc_res = 'OK';
              end if;

            else
              loc_res = 'ID EXISTED';  
          end if;
            return loc_res;
    END;
$$;


ALTER FUNCTION public.newfeature_list(par_name character varying, par_hotel_features integer) OWNER TO postgres;

--
-- TOC entry 238 (class 1255 OID 52787)
-- Name: newfeedback(character varying, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION newfeedback(par_name character varying, par_comment text, par_hotel_id integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    loc_res TEXT;
  BEGIN
    if par_name = '' OR par_comment = '' THEN
      loc_res = 'Error';
    ELSE
      INSERT INTO Feedback(name, comment, created_date, is_active, hotel_id) VALUES (par_name, par_comment, current_timestamp, TRUE, par_hotel_id);

      loc_res = 'Feedback Created';
    END IF;
    RETURN loc_res;
  END;
$$;


ALTER FUNCTION public.newfeedback(par_name character varying, par_comment text, par_hotel_id integer) OWNER TO postgres;

--
-- TOC entry 260 (class 1255 OID 53405)
-- Name: newhotel(character varying, character varying, text, character varying, character varying, character varying, character varying, character varying, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION newhotel(par_hotelname character varying, par_website_url character varying, par_description text, par_email character varying, par_paypal character varying, par_password character varying, par_address character varying, par_contact character varying, par_no_ofrooms integer, par_extra text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
        loc_hotelname VARCHAR;
        loc_res TEXT;
    BEGIN
    SELECT INTO loc_hotelname par_hotelname FROM Hotel WHERE hotel_name = par_hotelname;
      if loc_hotelname isnull THEN

    if par_email = '' or par_hotelname = ''  THEN
      loc_res = 'Error';

    ELSE
      INSERT INTO Hotel (hotel_name, website_url, description, email_address, paypal_account, password, address, contact_number,
                         no_of_rooms, extra, is_verified, is_hotel_admin, is_active) VALUES (par_hotelname, par_website_url, par_description, par_email,
                        par_paypal, par_password, par_address, par_contact, par_no_ofrooms,
                        par_extra, FALSE, TRUE, TRUE);
        loc_res = 'OK';
        end if;

     ELSE
        loc_res = 'ID EXISTED';

        end if;
        return loc_res;

END;
$$;


ALTER FUNCTION public.newhotel(par_hotelname character varying, par_website_url character varying, par_description text, par_email character varying, par_paypal character varying, par_password character varying, par_address character varying, par_contact character varying, par_no_ofrooms integer, par_extra text) OWNER TO postgres;

--
-- TOC entry 258 (class 1255 OID 52788)
-- Name: newhotel(character varying, character varying, text, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION newhotel(par_hotelname character varying, par_website_url character varying, par_description text, par_email character varying, par_paypal character varying, par_password character varying, par_address character varying, par_contact character varying, par_map_latitude character varying, par_longitude character varying, par_no_ofrooms integer, par_extra text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
        loc_hotelname VARCHAR;
        loc_res TEXT;
    BEGIN
    SELECT INTO loc_hotelname par_hotelname FROM Hotel WHERE hotel_name = par_hotelname;
      if loc_hotelname isnull THEN

    if par_email = '' or par_hotelname = ''  THEN
      loc_res = 'Error';

    ELSE
      INSERT INTO Hotel (hotel_name, website_url, description, email_address, paypal_account, password, address, contact_number, map_latitude,
                        map_longitude, no_of_rooms, extra, is_verified, is_hotel_admin, is_active) VALUES (par_hotelname, par_website_url, par_description, par_email,
                        par_paypal, par_password, par_address, par_contact, par_map_latitude, par_longitude, par_no_ofrooms,
                        par_extra, FALSE, TRUE, TRUE);
        loc_res = 'OK';
        end if;

     ELSE
        loc_res = 'ID EXISTED';

        end if;
        return loc_res;

END;
$$;


ALTER FUNCTION public.newhotel(par_hotelname character varying, par_website_url character varying, par_description text, par_email character varying, par_paypal character varying, par_password character varying, par_address character varying, par_contact character varying, par_map_latitude character varying, par_longitude character varying, par_no_ofrooms integer, par_extra text) OWNER TO postgres;

--
-- TOC entry 239 (class 1255 OID 52789)
-- Name: newhotel_features(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION newhotel_features(par_name character varying, par_hotel_id integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
    DECLARE
        loc_name TEXT;
        loc_res TEXT;
    BEGIN
        if par_name = '' THEN
        loc_res = 'Error';
        else
            select into loc_name name from Hotel_features where name = par_name;
            if loc_name isnull THEN
                insert into Hotel_features(name, hotel_id) values (par_name, par_hotel_id);
                loc_res = 'OK';

            else
                loc_res = 'ID EXISTED';
            end if;
        end if;
            return loc_res;
    END;
$$;


ALTER FUNCTION public.newhotel_features(par_name character varying, par_hotel_id integer) OWNER TO postgres;

--
-- TOC entry 263 (class 1255 OID 52790)
-- Name: newhotel_personnel(text, text, text, text, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION newhotel_personnel(par_fname text, par_mname text, par_lname text, par_email text, par_password text, par_hotel_id integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
        loc_email_address TEXT;
        loc_res TEXT;

    BEGIN
      SELECT INTO loc_email_address email FROM Hotel_Personnel WHERE email = par_email;
        if loc_email_address isnull THEN

      if par_email = '' OR par_fname='' OR par_mname = '' OR par_lname ='' OR par_password = '' THEN
        loc_res = 'Error';

      ELSE
          INSERT INTO Hotel_Personnel( fname, mname, lname, email, personnel_password, hotel_id) 
             VALUES(par_fname, par_mname, par_lname, par_email, par_password, par_hotel_id);
             loc_res = 'OK';
          end if;

        ELSE 
          loc_res = 'ID EXISTED';
        end if;

        return loc_res;
    END;
$$;


ALTER FUNCTION public.newhotel_personnel(par_fname text, par_mname text, par_lname text, par_email text, par_password text, par_hotel_id integer) OWNER TO postgres;

--
-- TOC entry 267 (class 1255 OID 61836)
-- Name: newimage(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION newimage(par_img character varying, par_hotel_id integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    loc_img varchar;
    loc_res Text;

    BEGIN
      SELECT INTO loc_img img FROM Image where img = par_img;
      if loc_img isnull THEN

        INSERT INTO Image(img, hotel_id) VALUES (par_img, par_hotel_id);
        loc_res = 'OK';

        ELSE
        loc_res = 'ID EXISTED';

        end if;
          return loc_res;

    END;
$$;


ALTER FUNCTION public.newimage(par_img character varying, par_hotel_id integer) OWNER TO postgres;

--
-- TOC entry 240 (class 1255 OID 52791)
-- Name: newimage(character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION newimage(par_img character varying, par_hotel_id integer, par_room_id integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    loc_img varchar;
    loc_res Text;

    BEGIN
      SELECT INTO loc_img img FROM Image where img = par_img;
      if loc_img isnull THEN

        INSERT INTO Image(img, hotel_id, room_id) VALUES (par_img, par_hotel_id, par_room_id);
        loc_res = 'OK';

        ELSE
        loc_res = 'ID EXISTED';

        end if;
          return loc_res;

    END;
  $$;


ALTER FUNCTION public.newimage(par_img character varying, par_hotel_id integer, par_room_id integer) OWNER TO postgres;

--
-- TOC entry 256 (class 1255 OID 52792)
-- Name: newroom(character varying, integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION newroom(par_room_type character varying, par_cost integer, par_max_person integer, par_total_room integer, par_hotel_id integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
    DECLARE
        loc_room_type TEXT;
        loc_hotel_id INT;
        loc_res TEXT;
    BEGIN
    SELECT INTO loc_room_type room_type, loc_hotel_id hotel_id FROM Room WHERE room_type = par_room_type and hotel_id = par_hotel_id;
    if loc_room_type isnull and loc_hotel_id ISNULL THEN

  if par_room_type = '' or par_cost IS NULL or par_max_person IS NULL or par_total_room IS NULL THEN
    loc_res = 'Error';

  ELSE
    INSERT INTO ROOM (room_type,cost, max_person, total_room, hotel_id) VALUES
    (par_room_type, par_cost, par_max_person, par_total_room, par_hotel_id);
    loc_res = 'OK';
  end if;

      ELSE
        loc_res = 'ID EXISTED';
      end if;

      return loc_res;
  END;
$$;


ALTER FUNCTION public.newroom(par_room_type character varying, par_cost integer, par_max_person integer, par_total_room integer, par_hotel_id integer) OWNER TO postgres;

--
-- TOC entry 242 (class 1255 OID 52793)
-- Name: newsystem_admin(character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION newsystem_admin(par_admin_fname character varying, par_admin_lname character varying, par_admin_email_add character varying, par_admin_password character varying) RETURNS text
    LANGUAGE plpgsql
    AS $$
	    DECLARE
		      loc_email_add VARCHAR(50);
		      loc_res TEXT;
	    BEGIN
       SELECT INTO loc_email_add email_address FROM System_admin WHERE email_address = par_admin_email_add;
          if loc_email_add isnull THEN

	        if par_admin_fname = '' OR par_admin_lname = '' THEN
	         loc_res = 'Error';
	        
          ELSE
		            INSERT INTO System_admin (fname, lname, email_address, password)
									VALUES (par_admin_fname, par_admin_lname, par_admin_email_add, par_admin_password);
      		        loc_res = 'OK';
                end if;

		          ELSE
                  loc_res = 'ID EXISTED';
		          end if;
		              return loc_res;
      END;
$$;


ALTER FUNCTION public.newsystem_admin(par_admin_fname character varying, par_admin_lname character varying, par_admin_email_add character varying, par_admin_password character varying) OWNER TO postgres;

--
-- TOC entry 266 (class 1255 OID 53418)
-- Name: newtransaction(character varying, character varying, character varying, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION newtransaction(par_payment_id character varying, par_checkin character varying, par_checkout character varying, par_fee integer, par_hotel_id integer, par_room_id integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
  loc_payment_id  VARCHAR;
  loc_year        INT;
  loc_random      INT;
  loc_transaction VARCHAR;
  loc_res         TEXT;

BEGIN
  SELECT INTO loc_payment_id payment_id
  FROM Transaction
  WHERE payment_id = par_payment_id;
  IF loc_payment_id ISNULL
  THEN
    loc_year = 2016;
    loc_random = ROUND(RANDOM() * 10000);
    loc_transaction = loc_year || '-' || loc_random;
    INSERT INTO transaction (transaction_number, payer_id, payment_id, paypal_token, name, contact_number,
                             email_address, checkin_date, checkout_date, fee, hotel_id, room_id)
    VALUES (loc_transaction, '', par_payment_id, '', '', 0, '', to_date(par_checkin, 'YYYY-MM-DD'), to_date(par_checkout, 'YYYY-MM-DD'), par_fee, par_hotel_id, par_room_id);
    loc_res ='OK';

  ELSE
    loc_res = 'Transaction exist';
  END IF;
  RETURN loc_res;
END;
$$;


ALTER FUNCTION public.newtransaction(par_payment_id character varying, par_checkin character varying, par_checkout character varying, par_fee integer, par_hotel_id integer, par_room_id integer) OWNER TO postgres;

--
-- TOC entry 259 (class 1255 OID 53404)
-- Name: update_hotel(integer, character varying, character varying, text, character varying, character varying, character varying, character varying, character varying, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION update_hotel(par_id integer, par_hotelname character varying, par_website_url character varying, par_description text, par_email character varying, par_paypal character varying, par_password character varying, par_address character varying, par_contact character varying, par_no_ofrooms integer, par_extra text) RETURNS void
    LANGUAGE sql
    AS $$
  UPDATE Hotel
    SET
      hotel_name = par_hotelname,
      website_url = par_website_url,
      description = par_description,
      email_address = par_email,
      paypal_account = par_paypal,
      password = par_password,
      address = par_address,
      contact_number = par_contact,
      no_of_rooms = par_no_ofrooms,
      extra = par_extra


    WHERE id_hotel = par_id;
$$;


ALTER FUNCTION public.update_hotel(par_id integer, par_hotelname character varying, par_website_url character varying, par_description text, par_email character varying, par_paypal character varying, par_password character varying, par_address character varying, par_contact character varying, par_no_ofrooms integer, par_extra text) OWNER TO postgres;

--
-- TOC entry 243 (class 1255 OID 52795)
-- Name: update_hotel(integer, character varying, character varying, text, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION update_hotel(par_id integer, par_hotelname character varying, par_website_url character varying, par_description text, par_email character varying, par_paypal character varying, par_password character varying, par_address character varying, par_contact character varying, par_map_latitude character varying, par_longitude character varying, par_no_ofrooms integer, par_extra text) RETURNS void
    LANGUAGE sql
    AS $$

    UPDATE Hotel
    SET
      hotel_name = par_hotelname,
      website_url = par_website_url,
      description = par_description,
      email_address = par_email,
      paypal_account = par_paypal,
      password = par_password,
      address = par_address,
      contact_number = par_contact,
      map_latitude = par_map_latitude,
      map_longitude = par_longitude,
      no_of_rooms = par_no_ofrooms,
      extra = par_extra


    WHERE id_hotel = par_id;

  $$;


ALTER FUNCTION public.update_hotel(par_id integer, par_hotelname character varying, par_website_url character varying, par_description text, par_email character varying, par_paypal character varying, par_password character varying, par_address character varying, par_contact character varying, par_map_latitude character varying, par_longitude character varying, par_no_ofrooms integer, par_extra text) OWNER TO postgres;

--
-- TOC entry 252 (class 1255 OID 52796)
-- Name: update_transaction(character varying, character varying, character varying, character varying, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION update_transaction(par_payment_id character varying, par_payer_id character varying, par_token character varying, par_name character varying, par_email character varying, par_contact_number integer) RETURNS void
    LANGUAGE sql
    AS $$

    UPDATE transaction
  SET
    payer_id       = par_payer_id,
    paypal_token   = par_token,
    name           = par_name,
    email_address  = par_email,
    contact_number = par_contact_number
  WHERE payment_id = par_payment_id;
 
$$;


ALTER FUNCTION public.update_transaction(par_payment_id character varying, par_payer_id character varying, par_token character varying, par_name character varying, par_email character varying, par_contact_number integer) OWNER TO postgres;

--
-- TOC entry 244 (class 1255 OID 52797)
-- Name: updatebusinessinfo(integer, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION updatebusinessinfo(par_id_info integer, par_license character varying, par_taxnum character varying, par_proof_pic character varying) RETURNS void
    LANGUAGE sql
    AS $$
  UPDATE business_information
    SET
      business_license = par_license,
      tax_identification_number = par_taxnum,
      proof_picture = par_proof_pic
    WHERE id_info = par_id_info;
  $$;


ALTER FUNCTION public.updatebusinessinfo(par_id_info integer, par_license character varying, par_taxnum character varying, par_proof_pic character varying) OWNER TO postgres;

--
-- TOC entry 245 (class 1255 OID 52798)
-- Name: updatefeatures_list(integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION updatefeatures_list(par_id_hotel_features_list integer, par_name character varying, par_hotel_features integer) RETURNS void
    LANGUAGE sql
    AS $$
    UPDATE Hotel_features_list
    SET
      name = par_name,
      hotel_features = par_hotel_features

    WHERE id_hotel_features_list = par_id_hotel_features_list;
$$;


ALTER FUNCTION public.updatefeatures_list(par_id_hotel_features_list integer, par_name character varying, par_hotel_features integer) OWNER TO postgres;

--
-- TOC entry 246 (class 1255 OID 52799)
-- Name: updatehotel_features(integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION updatehotel_features(par_id_hotel_features integer, par_name character varying, par_hotel_id integer) RETURNS void
    LANGUAGE sql
    AS $$
    UPDATE Hotel_features
    SET
      name = par_name,
      hotel_id = par_hotel_id

    WHERE id_hotel_features = par_id_hotel_features;
$$;


ALTER FUNCTION public.updatehotel_features(par_id_hotel_features integer, par_name character varying, par_hotel_id integer) OWNER TO postgres;

--
-- TOC entry 247 (class 1255 OID 52800)
-- Name: updateimage(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION updateimage(par_id_img integer, par_img character varying) RETURNS void
    LANGUAGE sql
    AS $$

  UPDATE Image
    SET
      img = par_img
    WHERE id_image = par_id_img;

  $$;


ALTER FUNCTION public.updateimage(par_id_img integer, par_img character varying) OWNER TO postgres;

--
-- TOC entry 248 (class 1255 OID 52801)
-- Name: updatepersonnel(integer, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION updatepersonnel(par_id_personnel integer, par_fname character varying, par_mname character varying, par_lname character varying, par_email_address character varying, par_personnel_password character varying) RETURNS void
    LANGUAGE sql
    AS $$

        UPDATE Hotel_Personnel
        SET
          fname = par_fname,
          mname = par_mname,
          lname = par_lname,
          email = par_email_address,
          personnel_password = par_personnel_password

        WHERE id_personnel = par_id_personnel;
$$;


ALTER FUNCTION public.updatepersonnel(par_id_personnel integer, par_fname character varying, par_mname character varying, par_lname character varying, par_email_address character varying, par_personnel_password character varying) OWNER TO postgres;

--
-- TOC entry 249 (class 1255 OID 52802)
-- Name: updateroom(integer, character varying, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION updateroom(par_id_room integer, par_room_type character varying, par_cost integer, par_max_person integer, par_total_room integer) RETURNS void
    LANGUAGE sql
    AS $$
    UPDATE Room
    SET 
      room_type = par_room_type,
      cost = par_cost,
      max_person = par_max_person,
      total_room = par_total_room

    WHERE id_room = par_id_room
  $$;


ALTER FUNCTION public.updateroom(par_id_room integer, par_room_type character varying, par_cost integer, par_max_person integer, par_total_room integer) OWNER TO postgres;

--
-- TOC entry 250 (class 1255 OID 52803)
-- Name: updatesystemadmin(integer, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION updatesystemadmin(par_id_admin integer, par_admin_fname character varying, par_admin_lname character varying, par_admin_email_add character varying, par_admin_password character varying) RETURNS void
    LANGUAGE sql
    AS $$
		UPDATE System_admin
		SET
			fname = par_admin_fname,
			lname = par_admin_lname,
			email_address = par_admin_email_add,
			password = par_admin_password

		WHERE id_admin = par_id_admin;
$$;


ALTER FUNCTION public.updatesystemadmin(par_id_admin integer, par_admin_fname character varying, par_admin_lname character varying, par_admin_email_add character varying, par_admin_password character varying) OWNER TO postgres;

--
-- TOC entry 257 (class 1255 OID 53390)
-- Name: verifypassword(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION verifypassword(par_password text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
  loc_password TEXT;
BEGIN
  loc_password= md5(par_password);
  
  RETURN loc_password;
END;
$$;


ALTER FUNCTION public.verifypassword(par_password text) OWNER TO postgres;

--
-- TOC entry 251 (class 1255 OID 52804)
-- Name: views(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION views(par_hote_id integer) RETURNS void
    LANGUAGE sql
    AS $$

    UPDATE visited
    SET
      count = count + 1
    WHERE hotel_id = par_hote_id;

  $$;


ALTER FUNCTION public.views(par_hote_id integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 201 (class 1259 OID 52739)
-- Name: business_information; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE business_information (
    id_info integer NOT NULL,
    business_license character varying(255),
    tax_identification_number character varying(255),
    proof_picture character varying(255),
    hotel_id integer
);


ALTER TABLE business_information OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 52737)
-- Name: business_information_id_info_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE business_information_id_info_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE business_information_id_info_seq OWNER TO postgres;

--
-- TOC entry 2303 (class 0 OID 0)
-- Dependencies: 200
-- Name: business_information_id_info_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE business_information_id_info_seq OWNED BY business_information.id_info;


--
-- TOC entry 191 (class 1259 OID 52667)
-- Name: feedback; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE feedback (
    feedback_id integer NOT NULL,
    name character varying(50),
    comment text,
    created_date timestamp without time zone,
    is_active boolean DEFAULT true,
    hotel_id integer
);


ALTER TABLE feedback OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 52665)
-- Name: feedback_feedback_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE feedback_feedback_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feedback_feedback_id_seq OWNER TO postgres;

--
-- TOC entry 2304 (class 0 OID 0)
-- Dependencies: 190
-- Name: feedback_feedback_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE feedback_feedback_id_seq OWNED BY feedback.feedback_id;


--
-- TOC entry 181 (class 1259 OID 52592)
-- Name: hotel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE hotel (
    id_hotel integer NOT NULL,
    hotel_name character varying(50),
    website_url character varying(100),
    description text,
    email_address character varying(50),
    paypal_account character varying(50),
    password character varying(50),
    address character varying(250),
    contact_number character varying(50),
    no_of_rooms integer,
    extra text,
    is_verified boolean DEFAULT false,
    is_hotel_admin boolean DEFAULT true,
    is_active boolean DEFAULT true
);


ALTER TABLE hotel OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 52698)
-- Name: hotel_features; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE hotel_features (
    id_hotel_features integer NOT NULL,
    name character varying(100),
    hotel_id integer,
    is_active boolean DEFAULT true
);


ALTER TABLE hotel_features OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 52696)
-- Name: hotel_features_id_hotel_features_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hotel_features_id_hotel_features_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hotel_features_id_hotel_features_seq OWNER TO postgres;

--
-- TOC entry 2305 (class 0 OID 0)
-- Dependencies: 194
-- Name: hotel_features_id_hotel_features_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE hotel_features_id_hotel_features_seq OWNED BY hotel_features.id_hotel_features;


--
-- TOC entry 197 (class 1259 OID 52712)
-- Name: hotel_features_list; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE hotel_features_list (
    id_hotel_features_list integer NOT NULL,
    name character varying(100),
    hotel_features integer,
    is_active boolean DEFAULT true
);


ALTER TABLE hotel_features_list OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 52710)
-- Name: hotel_features_list_id_hotel_features_list_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hotel_features_list_id_hotel_features_list_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hotel_features_list_id_hotel_features_list_seq OWNER TO postgres;

--
-- TOC entry 2306 (class 0 OID 0)
-- Dependencies: 196
-- Name: hotel_features_list_id_hotel_features_list_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE hotel_features_list_id_hotel_features_list_seq OWNED BY hotel_features_list.id_hotel_features_list;


--
-- TOC entry 180 (class 1259 OID 52590)
-- Name: hotel_id_hotel_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hotel_id_hotel_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hotel_id_hotel_seq OWNER TO postgres;

--
-- TOC entry 2307 (class 0 OID 0)
-- Dependencies: 180
-- Name: hotel_id_hotel_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE hotel_id_hotel_seq OWNED BY hotel.id_hotel;


--
-- TOC entry 185 (class 1259 OID 52615)
-- Name: hotel_personnel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE hotel_personnel (
    id_personnel integer NOT NULL,
    fname character varying(50),
    mname character varying(50),
    lname character varying(50),
    email character varying(50),
    personnel_password character varying(50),
    is_active boolean DEFAULT true,
    is_personnel boolean DEFAULT true,
    hotel_id integer
);


ALTER TABLE hotel_personnel OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 52613)
-- Name: hotel_personnel_id_personnel_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hotel_personnel_id_personnel_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hotel_personnel_id_personnel_seq OWNER TO postgres;

--
-- TOC entry 2308 (class 0 OID 0)
-- Dependencies: 184
-- Name: hotel_personnel_id_personnel_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE hotel_personnel_id_personnel_seq OWNED BY hotel_personnel.id_personnel;


--
-- TOC entry 193 (class 1259 OID 52684)
-- Name: image; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE image (
    id_image integer NOT NULL,
    img character varying(100),
    hotel_id integer,
    is_active boolean DEFAULT true
);


ALTER TABLE image OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 52682)
-- Name: image_id_image_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE image_id_image_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE image_id_image_seq OWNER TO postgres;

--
-- TOC entry 2309 (class 0 OID 0)
-- Dependencies: 192
-- Name: image_id_image_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE image_id_image_seq OWNED BY image.id_image;


--
-- TOC entry 187 (class 1259 OID 52630)
-- Name: room; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE room (
    id_room integer NOT NULL,
    room_name character varying(50),
    cost integer,
    max_person integer,
    available_room integer,
    total_room integer,
    hotel_id integer,
    is_active boolean DEFAULT true
);


ALTER TABLE room OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 52628)
-- Name: room_id_room_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE room_id_room_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE room_id_room_seq OWNER TO postgres;

--
-- TOC entry 2310 (class 0 OID 0)
-- Dependencies: 186
-- Name: room_id_room_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE room_id_room_seq OWNED BY room.id_room;


--
-- TOC entry 183 (class 1259 OID 52606)
-- Name: system_admin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE system_admin (
    id_admin integer NOT NULL,
    fname character varying(50),
    lname character varying(50),
    email_address character varying(50),
    password character varying(50),
    is_system_admin boolean DEFAULT true
);


ALTER TABLE system_admin OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 52604)
-- Name: system_admin_id_admin_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE system_admin_id_admin_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE system_admin_id_admin_seq OWNER TO postgres;

--
-- TOC entry 2311 (class 0 OID 0)
-- Dependencies: 182
-- Name: system_admin_id_admin_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE system_admin_id_admin_seq OWNED BY system_admin.id_admin;


--
-- TOC entry 189 (class 1259 OID 52644)
-- Name: transaction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE transaction (
    id_transaction integer NOT NULL,
    transaction_number character varying(20),
    payer_id character varying(100),
    payment_id character varying(100),
    paypal_token character varying(255),
    name character varying(50),
    contact_number integer,
    email_address character varying(50),
    checkin_date date,
    checkout_date date,
    fee integer,
    confirmed boolean DEFAULT false,
    is_done boolean DEFAULT false,
    hotel_id integer,
    room_id integer
);


ALTER TABLE transaction OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 52642)
-- Name: transaction_id_transaction_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE transaction_id_transaction_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE transaction_id_transaction_seq OWNER TO postgres;

--
-- TOC entry 2312 (class 0 OID 0)
-- Dependencies: 188
-- Name: transaction_id_transaction_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE transaction_id_transaction_seq OWNED BY transaction.id_transaction;


--
-- TOC entry 199 (class 1259 OID 52726)
-- Name: visited; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE visited (
    id_visited integer NOT NULL,
    count integer,
    hotel_id integer
);


ALTER TABLE visited OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 52724)
-- Name: visited_id_visited_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE visited_id_visited_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE visited_id_visited_seq OWNER TO postgres;

--
-- TOC entry 2313 (class 0 OID 0)
-- Dependencies: 198
-- Name: visited_id_visited_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE visited_id_visited_seq OWNED BY visited.id_visited;


--
-- TOC entry 2124 (class 2604 OID 52818)
-- Name: id_info; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY business_information ALTER COLUMN id_info SET DEFAULT nextval('business_information_id_info_seq'::regclass);


--
-- TOC entry 2116 (class 2604 OID 52819)
-- Name: feedback_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY feedback ALTER COLUMN feedback_id SET DEFAULT nextval('feedback_feedback_id_seq'::regclass);


--
-- TOC entry 2104 (class 2604 OID 52820)
-- Name: id_hotel; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hotel ALTER COLUMN id_hotel SET DEFAULT nextval('hotel_id_hotel_seq'::regclass);


--
-- TOC entry 2120 (class 2604 OID 52821)
-- Name: id_hotel_features; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hotel_features ALTER COLUMN id_hotel_features SET DEFAULT nextval('hotel_features_id_hotel_features_seq'::regclass);


--
-- TOC entry 2122 (class 2604 OID 52822)
-- Name: id_hotel_features_list; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hotel_features_list ALTER COLUMN id_hotel_features_list SET DEFAULT nextval('hotel_features_list_id_hotel_features_list_seq'::regclass);


--
-- TOC entry 2109 (class 2604 OID 52823)
-- Name: id_personnel; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hotel_personnel ALTER COLUMN id_personnel SET DEFAULT nextval('hotel_personnel_id_personnel_seq'::regclass);


--
-- TOC entry 2118 (class 2604 OID 52824)
-- Name: id_image; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY image ALTER COLUMN id_image SET DEFAULT nextval('image_id_image_seq'::regclass);


--
-- TOC entry 2111 (class 2604 OID 52825)
-- Name: id_room; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY room ALTER COLUMN id_room SET DEFAULT nextval('room_id_room_seq'::regclass);


--
-- TOC entry 2106 (class 2604 OID 52826)
-- Name: id_admin; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY system_admin ALTER COLUMN id_admin SET DEFAULT nextval('system_admin_id_admin_seq'::regclass);


--
-- TOC entry 2114 (class 2604 OID 52827)
-- Name: id_transaction; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction ALTER COLUMN id_transaction SET DEFAULT nextval('transaction_id_transaction_seq'::regclass);


--
-- TOC entry 2123 (class 2604 OID 52828)
-- Name: id_visited; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visited ALTER COLUMN id_visited SET DEFAULT nextval('visited_id_visited_seq'::regclass);


--
-- TOC entry 2294 (class 0 OID 52739)
-- Dependencies: 201
-- Data for Name: business_information; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY business_information (id_info, business_license, tax_identification_number, proof_picture, hotel_id) FROM stdin;
4	oo	fff	oerjdo	1
\.


--
-- TOC entry 2314 (class 0 OID 0)
-- Dependencies: 200
-- Name: business_information_id_info_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('business_information_id_info_seq', 9, true);


--
-- TOC entry 2284 (class 0 OID 52667)
-- Dependencies: 191
-- Data for Name: feedback; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY feedback (feedback_id, name, comment, created_date, is_active, hotel_id) FROM stdin;
1	Neiell	Try	2016-03-17 18:01:24	t	1
\.


--
-- TOC entry 2315 (class 0 OID 0)
-- Dependencies: 190
-- Name: feedback_feedback_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('feedback_feedback_id_seq', 13, true);


--
-- TOC entry 2274 (class 0 OID 52592)
-- Dependencies: 181
-- Data for Name: hotel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY hotel (id_hotel, hotel_name, website_url, description, email_address, paypal_account, password, address, contact_number, no_of_rooms, extra, is_verified, is_hotel_admin, is_active) FROM stdin;
1	celadon	https://www.celadoniligan.com	affordable	celadon@yahoo.com.ph	celadon123	celadon121212	Iligan City	222-7681	100	Good Accommodation	f	t	t
3	kingsway	kingsway.com	inn	kingsway@iligan.com	lolo	churvabels	cagayan de oro city	023	30	hali na!	f	t	t
4	Ma. Christina	www.crtx.com	Go!	mxsd@sdmc.com	sd2SD@sd.com	f6d75c949cda2517b826cacba5523792	tagakilid	2323	23	haha	f	t	t
5	alamnaniya	www.crtx.com	Go!	koneb	sd2SD@sd.com	ddd7987dd2ce3455e90177318d99faaf	tagakilid	2323	23	haha	f	t	t
6	hugot	www.crtx.com	Go!	koneb@gmail.com	sd2SD@sd.com	ddd7987dd2ce3455e90177318d99faaf	tagakilid	2323	23	haha	f	t	t
7	bulad	www.crtx.com	Go!	koneb2000@gmail.com	sd2SD@sd.com	koneb	tagakilid	2323	23	haha	f	t	t
2	luxury	https://www.luxuryiligan.com	affordable	luxury@yahoo.com.ph	luxury123	luxury121212	sabayle street, Iligan City	222-7681	100	Good Accomodation	f	t	t
\.


--
-- TOC entry 2288 (class 0 OID 52698)
-- Dependencies: 195
-- Data for Name: hotel_features; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY hotel_features (id_hotel_features, name, hotel_id, is_active) FROM stdin;
5	Hotel Facilities	1	t
6	Languages Spoken	1	t
\.


--
-- TOC entry 2316 (class 0 OID 0)
-- Dependencies: 194
-- Name: hotel_features_id_hotel_features_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('hotel_features_id_hotel_features_seq', 12, true);


--
-- TOC entry 2290 (class 0 OID 52712)
-- Dependencies: 197
-- Data for Name: hotel_features_list; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY hotel_features_list (id_hotel_features_list, name, hotel_features, is_active) FROM stdin;
12	Spa	5	t
15	fitness room	5	t
\.


--
-- TOC entry 2317 (class 0 OID 0)
-- Dependencies: 196
-- Name: hotel_features_list_id_hotel_features_list_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('hotel_features_list_id_hotel_features_list_seq', 22, true);


--
-- TOC entry 2318 (class 0 OID 0)
-- Dependencies: 180
-- Name: hotel_id_hotel_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('hotel_id_hotel_seq', 7, true);


--
-- TOC entry 2278 (class 0 OID 52615)
-- Dependencies: 185
-- Data for Name: hotel_personnel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY hotel_personnel (id_personnel, fname, mname, lname, email, personnel_password, is_active, is_personnel, hotel_id) FROM stdin;
5	Marygrace	Pasco	Cabolbol	marygrace@ymail.com	haha	t	t	1
6	Kristel	Ahlaine	Pabillaran	pabillarankristel@ymail.com	asdasd	t	t	1
1	joiner	si	loreinne	kor@sd.com	asdasd	t	t	7
2	koneb	ni	ambot	hahaha@sdsd.com	asdasd	t	t	7
24	koneba	nai	ambsot	ha2haha@sdsd.com	asdasd	t	t	7
25	neiell	care	paradiang	koneb@gmail.com	asdasd	t	t	7
26	marj	jorie	dexter	lablab@gmail.com	asdasd	t	t	1
\.


--
-- TOC entry 2319 (class 0 OID 0)
-- Dependencies: 184
-- Name: hotel_personnel_id_personnel_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('hotel_personnel_id_personnel_seq', 26, true);


--
-- TOC entry 2286 (class 0 OID 52684)
-- Dependencies: 193
-- Data for Name: image; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY image (id_image, img, hotel_id, is_active) FROM stdin;
44	5610_13070211560013757273.jpg	7	t
45	5610_13070213580013760545.jpg	7	t
46	5610_14032012470018782981.jpg	7	t
47	5610_14032612590018860147.jpg	7	t
48	5610_14032612590018860152.jpg	7	t
49	5610_14032612590018860153.jpg	7	t
50	5610_14032711480018875027.jpg	7	t
51	5610_14042208230019146509.jpg	7	t
52	5610_14042208230019146510.jpg	7	t
53	8.jpg	2	t
54	12722706_1153004058045855_2001696111_o.jpg	7	t
55	12755316_1153003961379198_663970442_o.jpg	2	t
\.


--
-- TOC entry 2320 (class 0 OID 0)
-- Dependencies: 192
-- Name: image_id_image_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('image_id_image_seq', 55, true);


--
-- TOC entry 2280 (class 0 OID 52630)
-- Dependencies: 187
-- Data for Name: room; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY room (id_room, room_name, cost, max_person, available_room, total_room, hotel_id, is_active) FROM stdin;
1	Single	1000	5	20	20	1	t
3	Family	3000	15	29	20	3	t
2	Double	1000	10	20	20	2	t
4	Double	2000	10	20	20	1	t
5	Single	1500	2	20	20	3	t
17	Single	1000	4	20	20	2	t
\.


--
-- TOC entry 2321 (class 0 OID 0)
-- Dependencies: 186
-- Name: room_id_room_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('room_id_room_seq', 17, true);


--
-- TOC entry 2276 (class 0 OID 52606)
-- Dependencies: 183
-- Data for Name: system_admin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY system_admin (id_admin, fname, lname, email_address, password, is_system_admin) FROM stdin;
1	Marjorie	Buctolan	m@gmail.com	marj123	t
2	Marjorie	Buctolan	marjbuctolan@gmail.com	marjorie123	t
\.


--
-- TOC entry 2322 (class 0 OID 0)
-- Dependencies: 182
-- Name: system_admin_id_admin_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('system_admin_id_admin_seq', 2, true);


--
-- TOC entry 2282 (class 0 OID 52644)
-- Dependencies: 189
-- Data for Name: transaction; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY transaction (id_transaction, transaction_number, payer_id, payment_id, paypal_token, name, contact_number, email_address, checkin_date, checkout_date, fee, confirmed, is_done, hotel_id, room_id) FROM stdin;
54	2016-3660		PAY-38D57160JM107811UK5CHCKQ			0		0029-11-06	0029-11-06	1000	f	f	2	17
55	2016-3807		PAY-14H16248NJ6141630K5CHQLA			0		0029-11-06	0029-11-06	1000	f	f	1	1
\.


--
-- TOC entry 2323 (class 0 OID 0)
-- Dependencies: 188
-- Name: transaction_id_transaction_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('transaction_id_transaction_seq', 55, true);


--
-- TOC entry 2292 (class 0 OID 52726)
-- Dependencies: 199
-- Data for Name: visited; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY visited (id_visited, count, hotel_id) FROM stdin;
1	8	1
3	1	3
4	1	1
\.


--
-- TOC entry 2324 (class 0 OID 0)
-- Dependencies: 198
-- Name: visited_id_visited_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('visited_id_visited_seq', 4, true);


--
-- TOC entry 2148 (class 2606 OID 52747)
-- Name: business_information_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY business_information
    ADD CONSTRAINT business_information_pkey PRIMARY KEY (id_info);


--
-- TOC entry 2138 (class 2606 OID 52676)
-- Name: feedback_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY feedback
    ADD CONSTRAINT feedback_pkey PRIMARY KEY (feedback_id);


--
-- TOC entry 2126 (class 2606 OID 52817)
-- Name: hotel_email_address_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hotel
    ADD CONSTRAINT hotel_email_address_key UNIQUE (email_address);


--
-- TOC entry 2144 (class 2606 OID 52718)
-- Name: hotel_features_list_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hotel_features_list
    ADD CONSTRAINT hotel_features_list_pkey PRIMARY KEY (id_hotel_features_list);


--
-- TOC entry 2142 (class 2606 OID 52704)
-- Name: hotel_features_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hotel_features
    ADD CONSTRAINT hotel_features_pkey PRIMARY KEY (id_hotel_features);


--
-- TOC entry 2132 (class 2606 OID 52622)
-- Name: hotel_personnel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hotel_personnel
    ADD CONSTRAINT hotel_personnel_pkey PRIMARY KEY (id_personnel);


--
-- TOC entry 2128 (class 2606 OID 52603)
-- Name: hotel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hotel
    ADD CONSTRAINT hotel_pkey PRIMARY KEY (id_hotel);


--
-- TOC entry 2140 (class 2606 OID 52690)
-- Name: image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY image
    ADD CONSTRAINT image_pkey PRIMARY KEY (id_image);


--
-- TOC entry 2134 (class 2606 OID 52636)
-- Name: room_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY room
    ADD CONSTRAINT room_pkey PRIMARY KEY (id_room);


--
-- TOC entry 2130 (class 2606 OID 52612)
-- Name: system_admin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY system_admin
    ADD CONSTRAINT system_admin_pkey PRIMARY KEY (id_admin);


--
-- TOC entry 2136 (class 2606 OID 52654)
-- Name: transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id_transaction);


--
-- TOC entry 2146 (class 2606 OID 52731)
-- Name: visited_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visited
    ADD CONSTRAINT visited_pkey PRIMARY KEY (id_visited);


--
-- TOC entry 2158 (class 2606 OID 52748)
-- Name: business_information_hotel_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY business_information
    ADD CONSTRAINT business_information_hotel_id_fkey FOREIGN KEY (hotel_id) REFERENCES hotel(id_hotel);


--
-- TOC entry 2153 (class 2606 OID 52677)
-- Name: feedback_hotel_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY feedback
    ADD CONSTRAINT feedback_hotel_id_fkey FOREIGN KEY (hotel_id) REFERENCES hotel(id_hotel);


--
-- TOC entry 2155 (class 2606 OID 52705)
-- Name: hotel_features_hotel_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hotel_features
    ADD CONSTRAINT hotel_features_hotel_id_fkey FOREIGN KEY (hotel_id) REFERENCES hotel(id_hotel);


--
-- TOC entry 2156 (class 2606 OID 52719)
-- Name: hotel_features_list_hotel_features_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hotel_features_list
    ADD CONSTRAINT hotel_features_list_hotel_features_fkey FOREIGN KEY (hotel_features) REFERENCES hotel_features(id_hotel_features);


--
-- TOC entry 2149 (class 2606 OID 52623)
-- Name: hotel_personnel_hotel_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hotel_personnel
    ADD CONSTRAINT hotel_personnel_hotel_id_fkey FOREIGN KEY (hotel_id) REFERENCES hotel(id_hotel);


--
-- TOC entry 2154 (class 2606 OID 52691)
-- Name: image_hotel_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY image
    ADD CONSTRAINT image_hotel_id_fkey FOREIGN KEY (hotel_id) REFERENCES hotel(id_hotel);


--
-- TOC entry 2150 (class 2606 OID 52637)
-- Name: room_hotel_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY room
    ADD CONSTRAINT room_hotel_id_fkey FOREIGN KEY (hotel_id) REFERENCES hotel(id_hotel);


--
-- TOC entry 2151 (class 2606 OID 52655)
-- Name: transaction_hotel_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_hotel_id_fkey FOREIGN KEY (hotel_id) REFERENCES hotel(id_hotel);


--
-- TOC entry 2152 (class 2606 OID 52660)
-- Name: transaction_room_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_room_id_fkey FOREIGN KEY (room_id) REFERENCES room(id_room);


--
-- TOC entry 2157 (class 2606 OID 52732)
-- Name: visited_hotel_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visited
    ADD CONSTRAINT visited_hotel_id_fkey FOREIGN KEY (hotel_id) REFERENCES hotel(id_hotel);


--
-- TOC entry 2301 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-05-25 12:39:41

--
-- PostgreSQL database dump complete
--

