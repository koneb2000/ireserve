
--HOTEL

create or replace function newhotel(par_email VARCHAR,par_hotelname VARCHAR, par_website_url VARCHAR, par_description TEXT, par_paypal VARCHAR,
                                      par_password VARCHAR, par_address VARCHAR, par_contact VARCHAR, par_no_ofrooms INT, par_extra TEXT) returns TEXT AS
$$
   DECLARE
        loc_email VARCHAR;
        loc_res TEXT;
    BEGIN
      SELECT INTO loc_email par_email FROM Hotel WHERE email_address = par_email;
        if loc_email isnull THEN

      if par_email = '' or par_hotelname = '' or par_website_url = '' or par_description = '' or par_paypal = '' or par_password = ''
                      or par_address = '' or par_contact = '' or par_no_ofrooms IS NULL or par_extra = '' THEN
        loc_res = 'Error';

      ELSE
          INSERT INTO Hotel (email_address, hotel_name, website_url, description, paypal_account, password, address, contact_number, no_of_rooms,
                        extra, is_verified, is_hotel_admin, is_active)
                        VALUES (par_email, par_hotelname, par_website_url, par_description,
                        par_paypal, par_password, par_address, par_contact, par_no_ofrooms,
                        par_extra, FALSE, TRUE, TRUE);
                        loc_res = 'OK';
          end if;

        ELSE
          loc_res = 'Error';

        end if;
        return loc_res;

    END;
$$
    LANGUAGE 'plpgsql';

--SELECT newhotel( 'celadon@yahoo.com.ph', 'celadon', 'https://www.celadoniligan.com', 'affordable', 'celadon123', 'celadon121212', 'Iligan City', '222-7681', 100, 'Good Accommodation');
--('luxuryhotel@yahoo.com.ph', 'luxury hotel', 'https://www.luxuryiligan.com', 'affordable', '123', '121212', 'Iligan City', '222-1234', 100, 'Good Accomodation')

create or replace function gethotel(OUT INT, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR, OUT TEXT, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR,
                                  OUT VARCHAR, OUT INT, OUT TEXT, OUT BOOLEAN, OUT BOOLEAN, OUT BOOLEAN) RETURNS SETOF RECORD AS
$$

  SELECT id_hotel, email_address, hotel_name, website_url, description, paypal_account, password, address, contact_number,
      no_of_rooms, extra, is_verified, is_hotel_admin, is_active FROM Hotel;

$$
  LANGUAGE 'sql';

-- select * from gethotel();

create or replace function gethotel_id(In par_id Int ,OUT VARCHAR, OUT VARCHAR, OUT VARCHAR, OUT TEXT, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR,
                                  OUT INT, OUT TEXT, OUT BOOLEAN, OUT BOOLEAN, OUT BOOLEAN) RETURNS SETOF RECORD AS
$$

  SELECT email_address, hotel_name, website_url, description, paypal_account, password, address, contact_number,
      no_of_rooms, extra, is_verified, is_hotel_admin, is_active FROM Hotel WHERE id_hotel = par_id;
$$
  LANGUAGE 'sql';


create or replace function update_hotel(par_id Int, par_email varchar, par_hotelname VARCHAR, par_website_url VARCHAR, par_description TEXT, par_paypal VARCHAR,
                                      par_password VARCHAR, par_address VARCHAR, par_contact VARCHAR, par_map_latitude VARCHAR,
                                      par_longitude VARCHAR, par_no_ofrooms INT, par_extra TEXT) returns void as
  $$

    UPDATE Hotel
    SET
      email_address = par_email,
      hotel_name = par_hotelname,
      website_url = par_website_url,
      description = par_description,
      paypal_account = par_paypal,
      password = par_password,
      address = par_address,
      contact_number = par_contact,
      map_latitude = par_map_latitude,
      map_longitude = par_longitude,
      no_of_rooms = par_no_ofrooms,
      extra = par_extra


    WHERE id_hotel = par_id;

  $$
  LANGUAGE 'sql';

create or replace function deactivate_hotel(IN par_id Int) returns void as
$$
       UPDATE Hotel
       SET
         is_active = FALSE

       WHERE id_hotel = par_id;
$$
       LANGUAGE 'sql';


--END of Hotel

-- Feedback


create or replace function newfeedback(par_name VARCHAR, par_comment TEXT, par_hotel_id INT) returns TEXT AS
$$
  DECLARE
    loc_res TEXT;
  BEGIN
    if par_name = '' OR par_comment = '' THEN
      loc_res = 'Error';
    ELSE
      INSERT INTO Feedback(name, comment, created_date, is_active, hotel_id) VALUES (par_name, par_comment, current_timestamp, TRUE, par_hotel_id);

      loc_res = 'Feedback Created';
    END IF;
    RETURN loc_res;
  END;
$$
  LANGUAGE 'plpgsql';

create or replace function getfeedback(OUT INT, OUT TEXT, OUT TEXT, OUT TIMESTAMP, OUT BOOLEAN, IN par_hotel INT) RETURNS SETOF RECORD AS
$$
  SELECT feedback_id, name, comment, created_date, is_active
  FROM feedback
  WHERE hotel_id = par_hotel
  AND is_active = TRUE  ;
$$
  LANGUAGE 'sql';

create or replace function getfeedback_id(IN par_feedback_id INT, OUT TEXT, OUT TEXT, OUT TIMESTAMP, OUT BOOLEAN, OUT INT) RETURNS SETOF RECORD AS
$$
  SELECT name, comment, created_date, is_active, hotel_id FROM Feedback WHERE feedback_id = par_feedback_id;
$$
  LANGUAGE 'sql';

create or replace function deletefeedback(IN par_feedback_id INT) returns void AS
$$
  UPDATE feedback
  SET is_active = False
  WHERE feedback_id = par_feedback_id;
$$
  LANGUAGE 'sql';




-- Hotel Personnel
create or replace function newHotel_Personnel(par_fname text, par_mname text, par_lname text, par_email text, par_password text,
                                         par_hotel_id int) returns TEXT as
$$
    DECLARE
        loc_email TEXT;
        loc_res TEXT;

    BEGIN
      SELECT INTO loc_email email FROM Hotel_Personnel WHERE email = par_email;
        if loc_email isnull THEN

      if par_email = '' OR par_fname='' OR par_mname = '' OR par_lname ='' OR par_password = '' THEN
        loc_res = 'Error';

      ELSE
          INSERT INTO Hotel_Personnel(fname, mname, lname, email, personnel_password, hotel_id)
             VALUES(par_fname, par_mname, par_lname, par_email, par_password, par_hotel_id);
             loc_res = 'OK';
          end if;

        ELSE
          loc_res = 'ID ALREADY EXISTS';
        end if;

        return loc_res;
    END;
$$
    LANGUAGE 'plpgsql';

--select newHotel_Personnel ('Kristel', 'Ahlaine', 'Pabillaran', 'pabillarankristel@ymail.com', 'asdasd', 1);

create or replace function getHotel_Personnel(OUT INT, OUT TEXT, OUT TEXT, OUT TEXT, OUT TEXT, OUT TEXT, OUT BOOLEAN, OUT BOOLEAN, OUT INT) RETURNS SETOF RECORD AS

$$

  SELECT id_personnel, email, fname, mname, lname, personnel_password, is_active, is_personnel, hotel_id FROM Hotel_Personnel;

$$
  LANGUAGE 'sql';

--select * from getHotel_Personnel();

create or replace function getid_personnel(IN par_id INT, OUT TEXT, OUT TEXT, OUT TEXT, OUT TEXT, OUT TEXT, OUT BOOLEAN,
                                        OUT BOOLEAN, OUT INT) RETURNS SETOF RECORD AS
$$

  SELECT email, fname, mname, lname, personnel_password, is_active, is_personnel, hotel_id
     FROM Hotel_Personnel WHERE id_personnel = par_id;

$$
  LANGUAGE 'sql';

--select * from getid_personnel(1);

create or replace function updatePersonnel(par_id INT,par_fname varchar, par_mname varchar, par_lname varchar, par_email_address varchar, par_personnel_password varchar) returns void as
$$

        UPDATE Hotel_Personnel
        SET
          fname = par_fname,
          mname = par_mname,
          lname = par_lname,
          email_address = par_email_address,
          personnel_password = par_personnel_password

        WHERE id_personnel = par_id;
$$
    LANGUAGE 'sql';

create or replace function deactivate(IN par_id INT) returns void as
$$
       UPDATE Hotel_Personnel
       SET
         is_active = FALSE

       WHERE id_personnel = par_id;
$$
       LANGUAGE 'sql';



CREATE OR REPLACE FUNCTION newroom(par_room_type varchar, par_cost int, par_max_person int, par_total_room int, par_hotel_id int)
  RETURNS text AS
$$
    DECLARE
        loc_room_type TEXT;
        loc_res TEXT;
    BEGIN
    SELECT INTO loc_room_type par_room_type FROM Room WHERE room_type = par_room_type;
    if loc_room_type isnull THEN

  if par_room_type = '' or par_cost IS NULL or par_max_person IS NULL or par_total_room IS NULL THEN
    loc_res = 'Error';

  ELSE
    INSERT INTO ROOM (room_type,cost, max_person, total_room, hotel_id) VALUES
    (par_room_type, par_cost, par_max_person, par_total_room, par_hotel_id);
    loc_res = 'OK';
  end if;

      ELSE
        loc_res = 'ID EXISTED';
      end if;

      return loc_res;
  END;
$$
  LANGUAGE 'plpgsql';

-- select newroom('Deluxe', 3000, 5, 25, 1);


create or replace function getroom(OUT Int,OUT Varchar, OUT Int, OUT Int, OUT INT, Out INT) RETURNS SETOF RECORD AS

$$

  SELECT id_room, room_type, cost, max_person, total_room, hotel_id FROM Room;

$$
  LANGUAGE 'sql';

--select * from getroom();

create or replace function getid_room(IN par_id_room Int, OUT varchar, OUT INT, OUT INT, OUT INT,OUT INT) RETURNS SETOF RECORD AS
$$

  SELECT room_type, cost, max_person, total_room, hotel_id
     FROM Room WHERE id_room = par_id_room;

$$
  LANGUAGE 'sql';

--select * from getid_room(1);

create or replace function updateroom(par_id_room Int, par_room_type varchar, par_cost int, par_max_person int, par_total_room int) returns void as
  $$
    UPDATE Room
    SET
      room_type = par_room_type,
      cost = par_cost,
      max_person = par_max_person,
      total_room = par_total_room

    WHERE id_room = par_id_room
  $$
  LANGUAGE 'sql';


-- Image

create or replace function newimage(par_img varchar, par_hotel_id INT, par_room_id int)returns text as
  $$
  DECLARE
    loc_img varchar;
    loc_res Text;

    BEGIN
      SELECT INTO loc_img img FROM Image where img = par_img;
      if loc_img isnull THEN

        INSERT INTO Image(img, hotel, room_id) VALUES (par_img, par_hotel_id, par_room_id);
        loc_res = 'OK';

        ELSE
        loc_res = 'ID EXISTED';

        end if;
          return loc_res;

    END;
  $$

  LANGUAGE 'plpgsql';

-- select newimage('ccc',1,1)


create or replace function getimage(OUT INT,OUT varchar, OUT INT, OUT INT) returns setof RECORD as
  $$

    SELECT id_image, img, hotel, room_id FROM Image;

  $$

  LANGUAGE 'sql';

-- select getimage()

create or replace function getimage_id(IN par_id_img INT, OUT VARCHAr, OUT INT, OUT INT) returns setof RECORD as
  $$

    SELECT img, hotel, room_id FROM Image WHERE id_image = par_id_img;

  $$
  LANGUAGE 'sql';

--select getimage_id(1)

create or replace function updateimage(par_id_img INT, par_img varchar) returns void as
  $$

  UPDATE Image
    SET
      img = par_img
    WHERE id_image = par_id_img;

  $$
  LANGUAGE 'sql';


-- business_information

create or replace function newbusinessinfo(par_license varchar, par_taxnum varchar, par_proof_pic varchar, par_hotel_id Int) returns TEXT as
  $$
  DECLARE
    loc_license Varchar;
    loc_res text;

    BEGIN

      SELECT INTO loc_license business_license FROM business_information WHERE business_license = par_license;
      if loc_license isnull THEN

      if par_license = '' or par_taxnum ='' or par_proof_pic ='' THEN
        loc_res = 'Error';

      ELSE
        INSERT INTO business_information(business_license,tax_identification_number,proof_picture, hotel_id)
            Values(par_license, par_taxnum, par_proof_pic, par_hotel_id);
            loc_res = 'OK';
          end if;

      ELSE
        loc_res = 'ID EXISTED';
      end if;

        return loc_res;
      END;
  $$
    LANGUAGE 'plpgsql';


-- select newbusinessinfo('oo', 'fff', 'oerjdo', 'celadon@yahoo.com.ph')


create or replace function getbusinessinfo(OUT VARCHAR, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR, OUT INT) returns setof RECORD as
  $$

      SELECT hotel_name, business_license, tax_identification_number, proof_picture, hotel_id FROM business_information INNER JOIN Hotel ON (business_information.hotel_id = Hotel.id_hotel)

  $$
  LANGUAGE 'sql';

-- select * from getbusinessinfo()


create or replace function getbusinessinfo_id(IN par_id_info INT, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR, OUT INT) returns setof RECORD as
  $$

      SELECT business_license, tax_identification_number, proof_picture, hotel_id FROM business_information WHERE id_info = par_id_info;

  $$

  LANGUAGE 'sql';

-- select getbusinessinfo_id(1)


create or replace function updatebusinessinfo(par_id_info INT, par_license VARCHAR, par_taxnum VARCHAR, par_proof_pic VARCHAR) returns void as
  $$
  UPDATE business_information
    SET
      business_license = par_license,
      tax_identification_number = par_taxnum,
      proof_picture = par_proof_pic
    WHERE id_info = par_id_info;
  $$
  LANGUAGE 'sql';


  --Transaction

CREATE OR REPLACE FUNCTION newtransaction(par_payment_id VARCHAR, par_checkin DATE, par_checkout DATE, par_fee INT,
                                          par_hotel_email   INT, par_room_id INT)
  RETURNS TEXT AS
$$
DECLARE
  loc_payment_id  VARCHAR;
  loc_year        INT;
  loc_random      INT;
  loc_transaction VARCHAR;
  loc_res         TEXT;

BEGIN
  SELECT INTO loc_payment_id payment_id
  FROM Transaction
  WHERE payment_id = par_payment_id;
  IF loc_payment_id ISNULL
  THEN
    loc_year = 2016;
    loc_random = ROUND(RANDOM() * 10000);
    loc_transaction = loc_year || '-' || loc_random;
    INSERT INTO transaction (transaction_number, payment_id, checkin_date, checkout_date, fee, hotel, room_id)
    VALUES (loc_transaction, par_payment_id, par_checkin, par_checkout, par_fee, par_hotel_email, par_room_id);
    loc_res ='OK';

  ELSE
    loc_res = 'Transaction exist';
  END IF;
  RETURN loc_res;
END;
$$
LANGUAGE 'plpgsql';
--SELECT newtransaction('232323', 'koneb', 2323, 'koneb@gmail.com', '2016-10-05', '2016-10-08', 200, 1, 1)

CREATE OR REPLACE FUNCTION update_transaction(par_payment_id VARCHAR, par_payer_id VARCHAR, par_token VARCHAR,
                                              par_name       VARCHAR, par_email VARCHAR, par_contact_number INT)
  RETURNS VOID AS
$$
UPDATE transaction
SET
  payer_id       = par_payer_id,
  paypal_token   = par_token,
  name           = par_name,
  email_address  = par_email,
  contact_number = par_contact_number
WHERE payment_id = par_payment_id;
$$
LANGUAGE 'sql';

--SELECT newtransaction('232323', 'koneb', 2323, 'koneb@gmail.com', '2016-10-05', '2016-10-08', 200, 1, 1)


CREATE or REPLACE FUNCTION confirm_transaction(par_payment_id VARCHAR) RETURNS VOID AS
  $$
    UPDATE transaction
    SET
      confirmed = TRUE
    WHERE payment_id = par_payment_id;
  $$
    LANGUAGE 'sql';

CREATE or REPLACE FUNCTION done_transaction(par_payment_id VARCHAR) RETURNS VOID AS
  $$
    UPDATE transaction
    SET
      is_done = TRUE
    WHERE payment_id = par_payment_id;
  $$
    LANGUAGE 'sql';


CREATE OR REPLACE FUNCTION gettransaction(IN par_hotel_id INT, OUT INT, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR,
                                          OUT             VARCHAR, OUT VARCHAR, OUT INT, OUT VARCHAR, OUT DATE,
                                          OUT             DATE, OUT INT, OUT INT, OUT BOOLEAN, OUT BOOLEAN)
  RETURNS SETOF RECORD AS
$$
SELECT
  id_transaction,
  transaction_number,
  payer_id,
  paypal_token,
  payment_id,
  name,
  contact_number,
  email_address,
  checkin_date,
  checkout_date,
  room_id,
  fee,
  confirmed,
  is_done
FROM Transaction
WHERE hotel= par_hotel_id;
$$
LANGUAGE 'sql';



CREATE OR REPLACE FUNCTION location_search(IN par_search VARCHAR, OUT INT, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR,
                                             OUT           VARCHAR, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR,
                                             OUT           INT, OUT TEXT, OUT VARCHAR, OUT INT, OUT INT, OUT INT, OUT INT)
    RETURNS SETOF RECORD AS
  $$
SELECT
  hotel.hotel_id,
  hotel.hotel_name,
  hotel.website_url,
  hotel.description,
  hotel.email_address,
  hotel.paypal_account,
  hotel.address,
  hotel.contact_number,
  hotel.map_latitude,
  hotel.map_longitude,
  hotel.no_of_rooms,
  hotel.extra,
  room.room_type,
  room.cost,
  room.max_person,
  room.available_room,
  room.total_room

FROM hotel
  INNER JOIN room ON hotel.hotel_id = room.hotel_id
WHERE lower(address) LIKE '%' || lower(par_search) || '%'
      OR lower(address) LIKE lower(par_search) || '%'
      OR lower(address) LIKE '%' || lower(par_search)
      AND hotel.is_active = TRUE;

  $$
    LANGUAGE 'sql';


--wla ni labot
CREATE OR REPLACE FUNCTION location_search_new(in par_search varchar, in par_hotel varchar, in par_roomtype varchar, in par_cost int4, out int4, out varchar, out varchar, out varchar, out varchar, out varchar, out varchar, out varchar, out varchar, out varchar, out int4, out text, out varchar, out int4, out int4, out int4, out int4)
  RETURNS SETOF RECORD AS
$$
  SELECT
  hotel.id_hotel,
  hotel.hotel_name,
  hotel.website_url,
  hotel.description,
  hotel.email_address,
  hotel.paypal_account,
  hotel.address,
  hotel.contact_number,
  hotel.map_latitude,
  hotel.map_longitude,
  hotel.no_of_rooms,
  hotel.extra,
  room.room_name,
  room.cost,
  room.max_person,
  room.available_room,
  room.total_room

FROM hotel
  INNER JOIN room ON hotel.id_hotel = room.hotel_id
WHERE
  CASE
  WHEN
    par_hotel NOTNULL
    THEN
      lower(hotel_name) LIKE '%' || lower(par_hotel) || '%'
      OR lower(hotel_name) LIKE lower(par_hotel) || '%'
      OR lower(hotel_name) LIKE '%' || lower(par_hotel)
  END
  and
    (lower(address) LIKE '%' || lower(par_search) || '%'
       OR lower(address) LIKE lower(par_search) || '%'
       OR lower(address) LIKE '%' || lower(par_search))
  AND hotel.is_active = TRUE
  and room.room_name = par_roomtype
ORDER BY hotel.hotel_name;

$$
  LANGUAGE 'sql';


CREATE OR REPLACE FUNCTION views(par_hotel_id INT)
  RETURNS VOID AS
$$
    UPDATE visited
    SET
      count = count + 1
    WHERE hotel = par_hotel_id;

  $$
LANGUAGE 'sql';


create or replace function newHotel_Features(par_name VARCHAR, par_hotel_id INT) returns TEXT as
$$
    DECLARE
        loc_name TEXT;
        loc_res TEXT;
    BEGIN
        if par_name = '' THEN
        loc_res = 'Error';
        else
            select into loc_name name from Hotel_features where name = par_name;
            if loc_name isnull THEN
                insert into Hotel_features(name, hotel_id) values (par_name, par_hotel_id);
                loc_res = 'OK';

            else
                loc_res = 'ID EXISTED';
            end if;
        end if;
            return loc_res;
    END;
$$
    LANGUAGE 'plpgsql';

--select newHotel_Features('Hotel Facilities', 1);


create or replace function getHotel_Features(out int, out varchar, out int, out boolean) returns setof record as
$$
    select id_hotel_features, name, hotel_id, is_active from Hotel_features;
$$
    LANGUAGE 'sql';

--select * from getHotel_Features();


create or replace function getid_features(in par_id_hotelfeatures int, out varchar, out int, out boolean) returns setof record as
$$
    select name, hotel_id, is_active from Hotel_features where id_hotel_features = par_id_hotelfeatures;
$$
    LANGUAGE 'sql';

--select * from getid_features(1);


create or replace function updateHotel_Features(par_id_hotel_features int, par_name varchar, par_hotel_id int) returns void as
$$
    UPDATE Hotel_features
    SET
      name = par_name,
      hotel_id = par_hotel_id

    WHERE id_hotel_features = par_id_hotel_features;
$$
    LANGUAGE 'sql';


create or replace function deactivate_hotelfeature(IN par_id_hotel_features int) returns void as
$$
   UPDATE Hotel_features
   SET
     is_active = FALSE

   WHERE id_hotel_features = par_id_hotel_features;
$$
   LANGUAGE 'sql';


-----------------------------Features List-------------------------------------
create or replace function newfeature_list(par_name varchar, par_hotel_features int) returns text as
$$
    DECLARE
        loc_name TEXT;
        loc_res TEXT;
    BEGIN
       select into loc_name name from Hotel_features_list where name = par_name;
            if loc_name isnull THEN

          if par_name = '' THEN
          loc_res = 'Error';
        else
          insert into Hotel_features_list(name, hotel_features) values (par_name, par_hotel_features);
              loc_res = 'OK';
              end if;

            else
              loc_res = 'ID EXISTED';
          end if;
            return loc_res;
    END;
$$
    LANGUAGE 'plpgsql';

--select newfeature_list('Spa', 4);


create or replace function getfeatures_list(out int, out varchar, out int, out boolean) returns setof record as
$$
    select id_hotel_features_list, name, hotel_features, is_active from Hotel_features_list;
$$
    LANGUAGE 'sql';

--select * from getfeatures_list();


create or replace function getid_featureslist(in par_id_hotel_features_list int, out varchar, out int, out boolean) returns setof record as
$$
    select name, hotel_features, is_active from Hotel_features_list where id_hotel_features_list = par_id_hotel_features_list;
$$
    LANGUAGE 'sql';

--select * from getid_featureslist(1);


create or replace function updateFeatures_List(par_id_hotel_features_list int, par_name varchar, par_hotel_features int) returns void as
$$
    UPDATE Hotel_features_list
    SET
      name = par_name,
      hotel_features = par_hotel_features

    WHERE id_hotel_features_list = par_id_hotel_features_list;
$$
    LANGUAGE 'sql';


create or replace function deactivate_featurelist(IN par_id_hotel_features_list int) returns void as
$$
   UPDATE Hotel_features_list
   SET
     is_active = FALSE

   WHERE id_hotel_features_list = par_id_hotel_features_list;
$$
   LANGUAGE 'sql';

create or REPLACE FUNCTION get_mostvisited() RETURNS SETOF RECORD AS
  $$

    SELECT count, hotel from visited ORDER BY count

  $$
    LANGUAGE 'sql';


create or replace function loginauth(in par_email text, in par_personnel_password text) returns text as
$$
  DECLARE
    loc_email text;
    loc_password text;
    loc_res text;
  BEGIN
    select into loc_email email_address from Hotel_Personnel where email_address = par_email;
    select into loc_password personnel_password from Hotel_Personnel where personnel_password = par_personnel_password;

    if loc_email isnull or loc_password isnull or loc_email = '' or loc_password = '' then
      loc_res = 'Invalid Email or Password';
    else
      loc_res = 'Successfully Logged In';
    end if;
    return loc_res;

  END;
$$
  LANGUAGE 'plpgsql';


create or replace function getpassword(par_email text) returns boolean as
$$
  declare
  loc_password text;
  begin
    select into loc_password personnel_password from Hotel_Personnel where email_address = par_email;
    if loc_password isnull then
      loc_password = 'null';
    end if;
    return loc_password;
  end;
$$
  LANGUAGE 'plpgsql';


create or replace function newSystem_admin(par_admin_email_add VARCHAR, par_admin_fname VARCHAR, par_admin_lname VARCHAR, par_admin_password VARCHAR) returns TEXT as

$$
      DECLARE
          loc_email_add VARCHAR(50);
          loc_res TEXT;
      BEGIN
       SELECT INTO loc_email_add email_address FROM System_admin WHERE email_address = par_admin_email_add;
          if loc_email_add isnull THEN

          if par_admin_fname = '' OR par_admin_lname = '' THEN
           loc_res = 'Error';

          ELSE
                INSERT INTO System_admin (email_address, fname, lname, password)
                  VALUES (par_admin_email_add, par_admin_fname, par_admin_lname, par_admin_password);
                  loc_res = 'OK';
                end if;

              ELSE
                  loc_res = 'ID EXISTED';
              end if;
                  return loc_res;
      END;
$$
  LANGUAGE 'plpgsql';

  --SELECT newSystem_admin('marjbuctolan@gmail.com', 'Marjorie', 'BUCTOLAN', 'marj123');



create or replace function getSystemAdmin(OUT INT, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR, OUT BOOLEAN) RETURNS SETOF RECORD AS

$$
  SELECT id_admin, email_address, fname, lname, password, is_system_admin FROM System_admin;

$$
  LANGUAGE 'sql';

  --SELECT * FROM getSystemAdmin();

create or replace function getAdmin_id(IN par_id INT, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR, OUT VARCHAR, OUT BOOLEAN) RETURNS SETOF RECORD AS

$$

  SELECT email_address, fname, lname, password, is_system_admin
    FROM System_admin WHERE id_admin = par_id;

$$
  LANGUAGE 'sql';


create or replace function updateSystemAdmin(par_id INT, par_email VARCHAR,par_admin_fname VARCHAR, par_admin_lname VARCHAR, par_admin_password VARCHAR) returns void as
$$
    UPDATE System_admin
    SET
      email_address = par_email,
      fname = par_admin_fname,
      lname = par_admin_lname,
      password = par_admin_password

    WHERE id_admin = par_id;
$$
  LANGUAGE 'sql';


create or replace function report_account(in par_hotel_id int) returns TEXT as
$$
  DECLARE
    loc_id int;
    loc_res text;

    BEGIN
      Select into loc_id par_hotel_id From Reported_accounts Where hotelid = par_hotel_id;
        if loc_id isnull Then

        Insert Into Reported_accounts (hotelid) Values (par_hotel_id);
          loc_res = 'Ok';

          ELSE
          loc_res = 'Already Reported';

          end if;
          return loc_res;
    END;

$$
LANGUAGE 'plpgsql';

create or replace function getreported_accounts(out varchar, out varchar, out int) returns setof record  as
$$
  Select hotel_name, email_address, hotelid from Reported_accounts INNER JOIN Hotel ON (Reported_accounts.hotelid = Hotel.hotel_id);
$$
LANGUAGE 'sql';


create or replace function report_feedback(in par_feedback_id int) returns TEXT as
$$
  DECLARE
    loc_id int;
    loc_res text;

    BEGIN
      Select into loc_id par_feedback_id From Reported_feedbacks Where feedbackid = par_feedback_id;
        if loc_id isnull Then

        Insert Into Reported_feedbacks (feedbackid) Values (par_feedback_id);
          loc_res = 'Ok';

          ELSE
          loc_res = 'Already Reported';

          end if;
          return loc_res;
    END;

$$
LANGUAGE 'plpgsql';

create or replace function getreported_feedbacks(out varchar, out text, out int) returns setof record  as
$$
  Select name, comment,feedbackid from Reported_feedbacks INNER JOIN Feedback ON (Reported_feedbacks.feedbackid = Feedback.hotel_id);
$$
LANGUAGE 'sql';

create or replace function verifyaccount(in par_id int) returns void AS
$$
  UPDATE Hotel
    SET
      is_verified = TRUE
    WHERE hotel_id = par_id;
$$

LANGUAGE 'sql';
