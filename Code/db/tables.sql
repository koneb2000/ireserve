CREATE TABLE Hotel (
  id_hotel SERIAL PRIMARY KEY,
  email_address Varchar(50),
  hotel_name Varchar(50),
  website_url VARCHAR(100),
  description TEXT,
  paypal_account VARCHAR(50),
  password VARCHAR(50),
  address Varchar(250),
  contact_number Varchar(50),
  no_of_rooms INT,
  extra TEXT,
  is_verified BOOLEAN DEFAULT FALSE,
  is_hotel_admin BOOLEAN DEFAULT TRUE,
  is_active BOOLEAN DEFAULT TRUE
);

CREATE TABLE System_admin (
  admin_id SERIAL PRIMARY KEY,
  email_address VARCHAR(50),
  fname VARCHAR(50),
  lname VARCHAR(50),
  password VARCHAR(50),
  is_system_admin BOOLEAN DEFAULT TRUE,
  is_active BOOLEAN DEFAULT TRUE
);

CREATE TABLE Hotel_Personnel (
 id_personnel SERIAL PRIMARY KEY,
 email VARCHAR(50),
 fname Varchar(50),
 mname Varchar(50),
 lname Varchar(50),
 personnel_password Varchar(50),
 is_active BOOLEAN DEFAULT TRUE,
 is_personnel BOOLEAN DEFAULT TRUE,
 hotel_id INT REFERENCES Hotel(id_hotel)
);


CREATE TABLE Room (
  id_room SERIAL PRIMARY KEY,
  room_type VARCHAR(50),
  cost INT,
  max_person  INT,
  available_room INT,
  total_room Int,
  hotel_id Int references Hotel(id_hotel),
  is_active BOOLEAN DEFAULT TRUE
);


CREATE TABLE Transaction (
  id_transaction      SERIAL PRIMARY KEY,
  transaction_number  VARCHAR(20),
  payer_id            VARCHAR(100) NULL,
  payment_id          VARCHAR(100),
  paypal_token        VARCHAR(255) NULL,
  name                VARCHAR(50),
  contact_number      INT,
  email_address       VARCHAR(50),
  transaction_date    DATE,
  checkin_date        DATE,
  checkout_date       DATE,
  fee                 INT,
  confirmed           BOOLEAN DEFAULT FALSE,
  is_done             BOOLEAN DEFAULT FALSE,
  hotel            Int REFERENCES Hotel(id_hotel),
  room_id             INT REFERENCES Room(id_room)
);



CREATE TABLE Feedback (
  feedback_id SERIAL PRIMARY KEY,
  name Varchar(50),
  comment TEXT,
  created_date TIMESTAMP,
  is_active BOOLEAN DEFAULT TRUE,
  hotel_id INT REFERENCES Hotel(id_hotel)
);

CREATE TABLE Image (
  id_image  SERIAL PRIMARY KEY,
  img VARCHAR(100),
  hotel  INT REFERENCES Hotel(id_hotel) null,
  room_id INT References Room(id_room) null,
  is_active BOOLEAN DEFAULT TRUE
);


CREATE TABLE Hotel_features (
  id_hotel_features SERIAL PRIMARY KEY,
  name VARCHAR(100),
  hotel_id INT REFERENCES Hotel(id_hotel),
  is_active BOOLEAN DEFAULT TRUE
);

CREATE TABLE Hotel_features_list (
  id_hotel_features_list SERIAL PRIMARY KEY,
  name VARCHAR(100),
  hotel_features INT REFERENCES Hotel_features (id_hotel_features),
  is_active BOOLEAN DEFAULT TRUE
);

CREATE TABLE business_information (
  id_info SERIAL PRIMARY KEY,
  business_license VARCHAR(255),
  tax_identification_number VARCHAR(255),
  proof_picture VARCHAR(255),
  hotel_id Int REFERENCES Hotel(id_hotel)
);

CREATE TABLE Reported_accounts (
 hotelid Int REFERENCES Hotel(id_hotel)
);

CREATE TABLE Reported_feedbacks (
 feedbackid Int REFERENCES Feedback(feedback_id)
);