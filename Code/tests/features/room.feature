Feature: Adding, Updating, and Retrieving room details


Scenario: Adding a room
		Given i want to add a room with the following details:
		|room_type | cost | max_person | total_room | hotel_id |
		| twin-bed | 3000 | 2 | 5 | 1 |
		When i add the room in the system
		Then i will get a '200' response
		And it should have a field "message" containing "OK" 

Scenario: Adding an existing room
		Given i want to add a room with the following details:
		|room_type | cost | max_person | total_room | hotel_id |
	    | Deluxe | 3000 | 5 | 25 | 1 |
		When i add the room in the system
		Then i will get a '200' response
		And it should have a field "message" containing "ID EXISTED"

Scenario: Adding a room with field room_type empty
		Given i want to add a room with the following details:
		|room_type | cost | max_person | total_room | hotel_id |
	    |  | 2000 | 5 | 90 | 1 |
	    When i add the room in the system
	    Then i will get a '200' response
	    And it should have a field "message" containing "error"



Scenario: Retrieve room's details
		Given a room with an id number '7'
		When I retrieve a room with an id number '7'
		Then I will get a '200' response
		And the following details are returned:
		|room_type | cost | max_person | total_room | hotel_id |
	    | Deluxe | 3000 | 5 | 25 | 1 |


Scenario: Retrieve a non-existent room
		Given a room with an id number '5'
		When i retrieve a room with an id number '5'
		Then it should have a field "status" containing "error"
		And it should have a field "message" containing "Results Not Found"


Scenario: Update room details 
		Given a room with an id number '2' with the following details:
		| room_type | cost | max_person | total_room | hotel_id |
	    | Single Room | 2000 | 10 | 30 | 1 |
	    When i update the room details into the following:
	    | room_type | cost | max_person | total_room | hotel_id |
	    | Single Room | 2000 | 10 | 35 | 1 |
	    Then i will get a '200' response
	    And it should have a field "status" containing "success"

