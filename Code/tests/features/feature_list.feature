Feature: Adding, Updating, Deactivating, and Retrieving hotel features


  Scenario: Add feature
    Given I have the feature details:
      | name | hotel_features |
      | Pool | 5 |
    When I click add button
    Then I will get a '200' response
    And it should have a field "message" containing "OK"


  Scenario: Add duplicate feature list
    Given I have the feature details:
      | name | hotel_features |
      | Spa | 5 |
    When I click add button
    Then I will get a '200' response
    And it should have a field "message" containing "ID EXISTED"


  Scenario: Add feature with empty name field
    Given I have the feature details:
      | name | hotel_features |
      |      | 4 |
    When I click add button
    Then I will get a '200' response
     And I should get a message containing 'Error' 


  Scenario: Retrieve features list
    Given hotel features id '12'
    When I retrieve the hotel features '12'
    Then I will get a '200' response
    And the following are returned:
      | id_hotel_features_list | name | hotel_features | is_active |
      | 12 | Spa | 5 | True |


  Scenario: Update feature list
    Given feature with the following details:
      | id_hotel_features_list | name | hotel_features | is_active |
      | 2 | Spa | 1 | True |
    When I update the details into the following:
      | id_hotel_features_list | name | hotel_features | is_active |
      | 2 | pool | 1 | True |
    Then I will get a '200' response
    And it should have a field "status" containing "success"


  Scenario: Deactivate hotel feature list
    Given hotel feature id '1' is a feature list
    When I click the deactivate
    Then I will get a '200' response
     And it should have a field "status" containing "success"