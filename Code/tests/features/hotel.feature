Feature: Handle storing, retrieving details, update details, deactivate a hotel


Scenario: Retrieve a hotel's details

    Given a hotel id '1'
    When I retrieve the hotel id '1'
    Then i will get a '200' response
    And the following hotel details are returned:
        | id_hotel | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | no_of_rooms | extra | is_verified | is_hotel_admin | is_active |
        | 1        | celadon@yahoo.com.ph | celadon | https://www.celadoniligan.com | affordable | celadon123 | celadon121212 | Iligan City | 222-7681 | 100 |  Good Accomodation | False | True | True |

Scenario: Signup Hotel
    Given I already fill up the hotel signup form:
        | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
        | luxuryhotel@yahoo.com.ph |  luxury hotel | https://www.luxuryiligan.com | affordable | 123 | 121212 | Iligan City | 222-1234 | 57623 | 1234 | 100            | Good Accomodation |
    When I submit the hotel signup form
    Then i will get a '200' response
    And it should have a field "status" containing "OK"

Scenario: Update a hotel
    Given the new hotel id '5' in database with the following details
        | hotel_id | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
        | 5 | luxuryhotel@yahoo.com.ph  | luxury hotel | https://www.luxuryiligan.com | affordable | luxury123 | luxury121212 | Iligan City | 222-7681 | 57623 | 1234    | 100        |  Good Accomodation     |
    When I update the hotel details into the following:
        | hotel_id | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
        | 5 | luxuryhotel@yahoo.com.ph | luxury hotel | https://www.luxuryiligan.com | The elegant hotel | luxury123 | 121212 | Iligan City | 222-7681 | 57623 | 1234    | 100        |  Good Accomodation     |
    Then I will get a '200' response
    And it should have a field "status" containing "OK"

Scenario: Deactivate hotel
    Given hotel id '5' is on the database
    When I click the deactivate button
    Then i will get a '200' response
    And it should have a field "status" containing "success"

Scenario: Adding a hotel that already exist
    Given I already fill up the hotel signup form:
        | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
        | celadon@yahoo.com.ph | celadon | https://www.celadoniligan.com | affordable | celadon123 | celadon121212 | Iligan City | 222-7681 | 57623 | 1234    | 100        |  Good Accomodation     |
    When I submit the hotel signup form
    Then i will get a '200' response
    And it should have a field "message" containing "Error"

Scenario: Sign up a hotel that has the field email_address is empty
    Given I already fill up the hotel signup form:
        | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
        | | oriobn | https://www.orion.com | weh | orion123 | orion121212 | Cagayan De Oro City | 222-0000 | 9990 | 555    | 90  |  whatever    |
    When I submit the hotel signup form
    Then i will get a '200' response
    Then a message 'Invalid Email address' are returned

Scenario: Sign up a hotel that has the field hotel_name is empty
    Given I already fill up the hotel signup form:
        | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | no_of_rooms | extra |
        |  celadon@yahoo.com.ph | | https://www.celadoniligan.com | affordable | celadon123 | celadon121212 | Iligan City | 222-7681 | 100        |  Good Accomodation     |
    When I submit the hotel signup form
    Then i will get a '200' response
    And it should have a field "message" containing "error"

Scenario: Sign up a hotel that has the field website_url is empty
    Given I already fill up the hotel signup form:
        | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
        |  celadon@yahoo.com.ph | oriobn | | affordable | celadon123 | celadon121212 | Iligan City | 222-7681 | 57623 | 1234    | 100        |  Good Accomodation     |
    When I submit the hotel signup form
    Then i will get a '200' response
    And it should have a field "message" containing "error"

Scenario: Sign up a hotel that has the field description is empty
    Given I already fill up the hotel signup form:
        | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
        |  celadon@yahoo.com.ph | oriobn | https://www.celadoniligan.com | | celadon123 | celadon121212 | Iligan City | 222-7681 | 57623 | 1234    | 100        |  Good Accomodation     |
    When I submit the hotel signup form
    Then i will get a '200' response
    And it should have a field "message" containing "error"

Scenario: Sign up a hotel that has the field paypal_account is empty
    Given I already fill up the hotel signup form:
        | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
        |  celadon@yahoo.com.ph | oriobn | https://www.celadoniligan.com | affordable | | celadon121212 | Iligan City | 222-7681 | 57623 | 1234    | 100        |  Good Accomodation     |
    When I submit the hotel signup form
    Then i will get a '200' response
    And it should have a field "message" containing "error"

Scenario: Sign up a hotel that has the field password is empty
    Given I already fill up the hotel signup form:
        | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
        |  celadon@yahoo.com.ph | oriobn | https://www.celadoniligan.com | affordable | celadon123 | | Iligan City | 222-7681 | 57623 | 1234    | 100        |  Good Accomodation     |
    When I submit the hotel signup form
    Then i will get a '200' response
    And it should have a field "message" containing "error"

Scenario: Sign up a hotel that has the field address is empty
    Given I already fill up the hotel signup form:
        | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
        |  celadon@yahoo.com.ph | oriobn | https://www.celadoniligan.com | affordable | celadon123 | celadon121212 | | 222-7681 | 57623 | 1234    | 100        |  Good Accomodation     |
    When I submit the hotel signup form
    Then i will get a '200' response
    And it should have a field "message" containing "error"

Scenario: Sign up a hotel that has the field contact_number is empty
    Given I already fill up the hotel signup form:
        | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
        |  celadon@yahoo.com.ph | oriobn | https://www.celadoniligan.com | affordable | celadon123 | celadon121212 | Iligan City | | 57623 | 1234    | 100        |  Good Accomodation     |
    When I submit the hotel signup form
    Then i will get a '200' response
    And it should have a field "message" containing "error"

Scenario: Sign up a hotel that has the field map_latitude is empty
    Given I already fill up the hotel signup form:
        | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
        |  celadon@yahoo.com.ph | oriobn | https://www.celadoniligan.com | affordable | celadon123 | celadon121212 | Iligan City | 222-7681 | | 1234    | 100        |  Good Accomodation     |
    When I submit the hotel signup form
    Then a message 'Error' are returned

Scenario: Sign up a hotel that has the field map_longitude is empty
    Given I already fill up the hotel signup form:
        | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
        |  celadon@yahoo.com.ph | oriobn | https://www.celadoniligan.com | affordable | celadon123 | celadon121212 | Iligan City | 222-7681 | 57623 | | 100        |  Good Accomodation     |
    When I submit the hotel signup form
    Then i will get a '200' response
    And it should have a field "message" containing "error"

#Scenario: Sign up a hotel that has the field no_of_rooms is empty
#    Given I already fill up the hotel signup form:
#        | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
#        |  celadon@yahoo.com.ph | oriobn | https://www.celadoniligan.com | affordable | celadon123 | celadon121212 | Iligan City | 222-7681 | 57623 | 1234 | |  Good Accomodation     |
#    When I submit the hotel signup form
#    Then i will get a '200' response
#    And it should have a field "message" containing "error"

Scenario: Sign up a hotel that has the field extra is empty
    Given I already fill up the hotel signup form:
        | email_address | hotel_name | website_url | description | paypal_account | password | address |contact_number | map_latitude | map_longitude | no_of_rooms | extra |
        |  celadon@yahoo.com.ph | oriobn | https://www.celadoniligan.com | affordable | celadon123 | celadon121212 | Iligan City | 222-7681 | 57623 | 1234 | 100 | |
    When I submit the hotel signup form
    Then i will get a '200' response
    And it should have a field "message" containing "error"