Feature: Add System Admin, retrieve system admin ID, Update System admin

  Scenario: Signup System Admin
    Given I already fill up the system admin signup form:
      | email_address | fname | lname | password |
      | m@gmail.com | Marjorie | Buctolan | marj123 |
    When I submit the signup form
    Then i will get a '200' response
    And it should have a field "status" containing "OK"


   Scenario: Retrieving a System Admin
    Given an admin id '1'
    When I retrieve the admin id '1'
    Then I will get a '200' response
    And the following system admin details are returned:
      | id_admin | email_address | fname | lname | is_system_admin |
      | 1 | m@gmail.com | Marjorie | Buctolan | True |


   Scenario: Updating System Admin
    Given the new system admin id "1" in database with the following details
        | email_address | fname | lname | password | is_system_admin | is_active |
        | marjbuctolan@gmail.com | Marjorie | Buctolan | marj123 | True | True   |
    When I update the system admin details into the following:
        | email_address | fname | lname | password | is_active |
        | marjbuctolan@gmail.com | Marj | Buctolan | marjorie123 | True |
    Then i will get a '200' response
    And it should have a field "status" containing "OK"


   Scenario: Adding an existing system admin
    Given I already fill up the system admin signup form:
      | email_address | fname | lname | password |
      | m@gmail.com | Marjorie | Buctolan | marj123 |
    When I submit the signup form
    Then i will get a '200' response
    And it should have a field "message" containing "ID EXISTED"

  Scenario: Sign up a system admin that has the field fname is empty
    Given I already fill up the system admin signup form:
        | email_address | fname | lname | password |
        |  bermejo@gmail.com |  | Buctolan | marjorie123 |
    When I submit the signup form
    Then i will get a '200' response
    And it should have a field "message" containing "error"