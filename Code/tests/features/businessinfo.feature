Feature: Adding, Updating, and Retrieving business information


Scenario: Adding new business information
		Given i input the following business information:
		| business_license | tax_identification_number | proof_pic | hotel_id |
		| lala| eofor | lolo | 1 |
		When i add the information in the system
		Then i will get a '200' response
		And it should have a field "message" containing "success"
        And it should have a field "status" containing "OK"


Scenario: Adding a business information with business license empty
		Given i input the following business information:
		| business_license | tax_identification_number | proof_pic | hotel_id |
		| | orio | biscuit | 1 |
		When i add the information in the system
		Then i will get a '200' response
        And it should have a field "message" containing "error"


Scenario: Adding a business information with tax_identification_number empty
		Given i input the following business information:
		| business_license | tax_identification_number | proof_pic | hotel_id |
		| milk |  | biscuit | 1 |
		When i add the information in the system
		Then i will get a '200' response
        And it should have a field "message" containing "error"


Scenario: Adding a business information with proof_pic empty
		Given i input the following business information:
		| business_license | tax_identification_number | proof_pic | hotel_id |
		| milk | orio |  | 1 |
		When i add the information in the system
		Then i will get a '200' response
        And it should have a field "message" containing "error"



Scenario: Retrieve business information
		Given a business information with an id number '4'
		When i retrieve the business information with id number '4'
		Then i will get a '200' response
		And the following business information is returned:
		| business_license | tax_identification_number | proof_pic | hotel_id |
		| oo | fff | oerjdo | 1 |


Scenario: Updating business information
		Given a business information with an id number '18'
		| business_license | tax_identification_number | proof_picture | hotel_id |
		| lala | eofor | lolo | 1 |
		When i update the business information to:
		| business_license | tax_identification_number | proof_picture | hotel_id |
		| LALA | EOFOR | LOLO | 1 |
		Then i will get a '200' response
	    And it should have a field "status" containing "success"


