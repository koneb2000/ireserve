Feature: Adding, Updating, and Retrieving hotel features


  Scenario: Add hotel feature
    Given I have the following hotel features details:
      | name | hotel_id |
      | churva | 1 |
    When I add the hotel feature in the system
    Then I will get a '200' response
    And it should have a field "message" containing "OK"

  Scenario: Add duplicate hotel feature
    Given I have the following hotel features details:
      | name | hotel_id |
      | Languages Spoken | 1 |
    When I add the hotel feature in the system
    Then I will get a '200' response
    And it should have a field "message" containing "ID EXISTED"

  Scenario: Add hotel feature with empty name field
    Given I have the following hotel features details:
      | name | hotel_id |
      |      | 1        |
    When I add the hotel feature in the system
    Then I will get a '200' response
    And it should have a field "message" containing "Error"

  Scenario: Retrieve hotel features
    Given hotel features with id '5'
    When I retrieve the hotel features with id '5'
    Then I will get a '200' response
    And the following hotel features are returned:
      | id_hotel_features | name | hotel_id | is_active |
      | 5 | Hotel Facilities | 1 | True |


  Scenario: Update hotel features details
    Given a hotel feature with the following details:
      | id_hotel_features | name | hotel_id | is_active |
      | 2 | Languages Spoken | 1 | True |
    When I update the details into the following:
      | id_hotel_features | name | hotel_id | is_active |
      | 2 | Sports and Recreation | 1       | True |
    Then I will get a '200' response
    And it should have a field "status" containing "success"


  Scenario: Deactivate hotel feature
    Given hotel feature id '1'
    When I trigger the deactivate button
    Then I will get a '200' response
    And it should have a field "status" containing "success"