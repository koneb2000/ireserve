Feature: Adding, Updating, and Retrieving image details


Scenario: Adding a new image
		Given i input the following details:
		| img | hotel_id | room_id |
		| kkk.jpeg | 1 | 7 |
		When i add the image in the system
		And it should have a field "message" containing "success"
        And it should have a field "status" containing "OK"


Scenario: Retrieving image details
		Given an image with id '7'
		When i retrieve the image with id '7'
		Then the following details is returned:
		| img | hotel_id | room_id |
		| kkk.jpeg | 1 | 7 |

Scenario: Retrieving a non-existent image
		Given an image with id '5'
		When i retrieve the image with id '5'
		Then i will get a '404' response
		And it should have a field "status" containing "error"
		And it should have a field "message" containing "Results Not Found"

Scenario: Updating an image
		Given an image exist with the following details:
		| img | hotel_id | room_id |
		| koko.jpeg | 1 | 7 |
		When i change the image details to:
		| img | hotel_id | room_id |
		| haha.png | 1 | 7 |
		Then i will get a '200' response
	    And it should have a field "status" containing "success"