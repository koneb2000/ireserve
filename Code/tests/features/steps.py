from lettuce import step, world, before
from nose.tools import assert_equals
from webtest import *
from app import app
from app.__init__ import *
import json
import base64

@before.all
def before_all():
    world.app = app.test_client()

    world.headers = {'Authorization': 'Basic %s' % (
        base64.b64encode("celadon@yahoo.com.ph:celadon121212"))}




"""Adding new room"""

@step(u'Given i want to add a room with the following details:')
def given_room_details(step):
    world.d = step.hashes[0]


@step(u'Then i will get a \'([^\']*)\' response')
def then_i_should_get_response(step, expected_status_code):
    assert_equals(world.response.status_code, int(expected_status_code))

@step(u'And it should have a field "message" containing "OK"')
def message_success(step):
    world.resp = json.loads(world.response.data)
    assert_equals(world.resp['message'], "OK")


"""Adding an existing room"""

@step(u'When i add the room in the system')
def add_the_room(step):
    world.browser = TestApp(app)
    world.room_url = '/api/v1.0/room/'
    world.response = world.app.post(world.room_url, data = json.dumps(world.d))


@step(u'And it should have a field "message" containing "ID EXISTED"')
def message_success(step):
    world.resp = json.loads(world.response.data)
    assert_equals(world.resp['message'], "ID EXISTED")


"""Adding a room with field room_type empty"""


@step(u'And it should have a field "message" containing "Error"')
def message_error(step):
    world.resp = json.loads(world.response.data)
    assert_equals(world.resp['message'], 'Error')



"""Retrieving room details"""

@step(u'Given a room with an id number \'(.*)\'')
def given_some_rooms_are_in_the_system(step,id):
    world.browser = TestApp(app)
    world.room = world.app.get('/api/v1.0/room/{}/'.format(id))
    world.resp = json.loads(world.room.data)

@step(u'When I retrieve a room with an id number \'(.*)\'')
def when_i_retrieve_the_room_number(step,id):
    world.response = world.app.get('/api/v1.0/room/{}/'.format(id))

    
@step(u'And the following details are returned:')
def details_returned(step):
    world.info = step.hashes[0]
    assert_equals(world.info, json.loads(world.response.data))


"""Retrieving a non-existent room"""


@step(u'Then it should have a field "status" containing "error"')
def response_status(step):
    world.resp = json.loads(world.response.data)
    assert_equals(world.resp['status'], "error")

@step(u'And it should have a field "message" containing "Results Not Found"')
def response_message(step):
    world.resp = json.loads(world.response.data)
    assert_equals(world.resp['message'], "Results Not Found")


"""Updating room details"""

@step(u'Given a room with an id number "3" with the following details:')
def given_the_following_details(step):
    world.details3 = step.hashes[0]

@step(u'When i update the room details into the following:')
def change_details(step):
    world.details4 = step.hashes[0]
    world.browser = TestApp(app)
    world.room_url = '/api/v1.0/room/'
    world.response = world.app.put(world.room_url, data = json.dumps(world.details4))


@step(u'And it should have a field "status" containing "success"')
def status_success(step):
    world.resp = json.loads(world.response.data)
    assert_equals(world.resp['status'], "success")




"""Adding new business information"""

@step(u'Given i input the following business information:')
def i_input_the_information(step):
    world.bf = step.hashes[0]

@step(u'When i add the information in the system')
def add_the_info(step):
    world.browser = TestApp(app)
    world.bf_url = '/api/v1.0/business_info/'
    world.response = world.app.post(world.bf_url, data = json.dumps(world.bf))


"""Retrieving business information"""

@step(u'Given a business information with an id number \'(.*)\'')
def id_number_exist(step,id):
    world.browser = TestApp(app)
    world.businfo = world.app.get('/api/v1.0/business_infos/{}/'.format(id))
    world.resp = json.loads(world.businfo.data)


@step(u'When i retrieve the business information with id number \'(.*)\'')
def retrieve_details(step,id):
    world.response = world.app.get('/api/v1.0/business_info/{}/'.format(id))


@step(u'And the following business information is returned:')
def get_details(step):
    assert_equals(step.hashes, [json.loads(world.response.data)])


@step(u'And it should have a field "message" containing "success"')
def message_field(step):
    world.rsp = json.loads(world.response.data)
    assert_equals(world.rsp['message'], "success")


@step(u'And it should have a field "status" containing "OK"')
def message_field(step):
    world.rsp = json.loads(world.response.data)
    assert_equals(world.rsp['status'], "OK")

@step(u'And it should have a field "status" containing "error"')
def response_status(step):
    world.resp = json.loads(world.response.data)
    assert_equals(world.resp['status'], "error")


"""updating business information"""

@step(u'Given a business information with id "3" exist with the following details:')
def given_the_following_details(step):
    world.biz = step.hashes[0]

@step(u'When i update the business information to:')
def change_details(step):
    world.biz1 = step.hashes[0]
    world.browser = TestApp(app)
    world.biz_url = '/api/v1.0/business_info/'
    world.response = world.app.put(world.biz_url, data = json.dumps(world.biz1))



"""Create Feedback"""

@step(u'Given I am in the hotel feedback form with details:')
def given_i_am_in_the_hotel_feedback_form_with_details(step):
    world.feedback = step.hashes[0]


@step(u'When I submit the feedback form')
def when_i_submit_the_feedback_form(step):
    world.browser = TestApp(app)
    world.response = world.browser.post('/api/v1.0/feedback/', world.feedback)


@step(u'And a message \'([^\']*)\' are returned')
def and_a_message_group1_are_returned(step, message):
    resp = json.loads(world.response.body)
    assert_equals(resp['message'], message)


"""View Feedback"""

@step(u'Given I am in the hotel\'s page with id:')
def given_i_am_in_the_hotel_s_page_with_id(step):
    world.hotel_id = step.hashes[0]['hotel_id']


@step(u'When I pressed view all feedback')
def when_i_pressed_view_all_feedback(step):
    world.browser = TestApp(app)
    world.response = world.browser.get('/api/v1.0/feedback/{}/'.format(world.hotel_id))


@step(u'And it returns all hotel\'s feedback:')
def and_it_shows_all_hotel_s_feedback(step):
    resp = json.loads(world.response.body)

    for column in step.hashes:
        world.comment = column['comment']
        world.created_date = column['created_date']
        world.id = column['id']
        world.name = column['name']

    col = [[{'comment': str(world.comment), 'created_date': str(world.created_date), 'id': int(world.id),
    'name': str(world.name)}
    ]]

    assert_equals(col, [resp['entries']])


"""Add new hotel personnel"""

@step(u'Given I have the following hotel personnel details:')
def personnel_details(step):
    world.personnel = step.hashes[0]


@step(u'When I click the register button')
def click_register_button(step):
    world.browser = TestApp(app)
    world.personnel_url = '/api/v1.0/hotelpersonnel/'
    world.response = world.app.post(world.personnel_url, data = json.dumps(world.personnel))

"""Add duplicate hotel personnel"""
@step(u'And it should have a field "message" containing "ID ALREADY EXISTS"')
def message_error(step):
    world.resp = json.loads(world.response.data)
    assert_equals(world.resp['message'], "ID ALREADY EXISTS")



"""Retrieving hotel personnel"""

@step(u'Given a hotel personnel id \'(.*)\'')
def hotel_personnel_retrieval(step,id_hotel):
    world.browser = TestApp(app)
    world.personnel_det = world.app.get('/api/v1.0/hotelpersonnels/{}/'.format(id_hotel))
    world.resp = json.loads(world.personnel_det.data)


@step(u'When I retrieve hotel personnel \'(.*)\'')
def retrieve_personnel(step,id_hotel):
    world.response = world.app.get('/api/v1.0/hotelpersonnels/{}/'.format(id_hotel))


@step(u'And the following hotel personnel details are returned:')
def and_the_following_hotel_personnel_details_are_returned(step):
    assert_equals(step.hashes, [json.loads(world.response.data)])


"""Updating hotel personnel details"""


@step(u'When I update the personnel details into the following:')
def update_details(step):
    world.update1 = step.hashes[0]
    world.browser = TestApp(app)
    world.personnel_url = '/api/v1.0/hotelpersonnel/update/'
    world.response = world.app.put(world.personnel_url, data = json.dumps(world.update1))


@step(u'And it should have a field "status" containing "success"')
def success(step):
    world.resp = json.loads(world.response.data)
    assert_equals(world.resp['status'], 'success')


"""Deactivate Hotel Personnel"""

@step(u'Given hotel personnel id \'(.*)\'')
def step_impl(step, id_personnel):
    world.id = id_personnel


@step(u'When I click the deactivate button')
def when_i_click_the_deactivate_button(step):
    world.browser = TestApp(app)
    world.response = world.app.put('/api/v1.0/hotelpersonnel/deactivate/{}/', world.id)


@step(u'And a message "Successfully Registered" is returned')
def success_message(step):
    world.resp = json.loads(world.response.data)
    assert_equals(world.resp['message'], "Successfully Registered")


@step(u'And I should get a message containing \'([^\']*)\'')
def error_msg(step,message):
    world.resp = json.loads(world.response.data)
    assert_equals(world.resp['message'], message)


"""Add a Hotel"""

@step(u'Given I already fill up the hotel signup form:')
def fill_up(step):
    world.fill = step.hashes[0]

@step(u'When I submit the hotel signup form')
def submit_form(step):
    world.browser = TestApp(app)
    world.hotel_uri = '/api/v1.0/hotels/signup/'
    world.response = world.app.post(world.hotel_uri, data = json.dumps(world.fill))

@step(u'Then a message \'(.*)\' are returned')
def error_message(step,message):
    resp = json.loads(world.response.data)
    assert_equals(resp['message'], message)

"""Retrieving a hotel"""

@step(u'Given a hotel id \'(.*)\'')
def Given_a_hotel_that_exists_with_the_following_details(step,hotel_id):
    world.browser = TestApp(app)
    world.hotel_det = world.app.get('/api/v1.0/hotels/{}/'.format(hotel_id))
    world.resp = json.loads(world.hotel_det.data)


@step(u'When I retrieve the hotel id \'(.*)\'')
def When_I_retrieve_the_hotel_id(step, hotel_id):
    world.response = world.app.get('/api/v1.0/hotels/{}/'.format(hotel_id))


@step(u"And the following hotel details are returned:")
def and_the_following_hotel_details_are_returned(step):
    # world.infos = step.hashes[0]
    # assert_equals(world.infos, json.loads(world.response.data))
    assert_equals(step.hashes, [json.loads(world.response.data)])
    # r= json.loads(world.resp.data)
    # assert_equals(world.r['entries'], r['entries'])


"""Deactivate a Hotel""" 

@step(u"Given hotel id \'(.*)\' is on the database")
def hotel_id_is_on_the_database(step, hotel_id):
    world.id = hotel_id

@step("When I click the deactivate button")
def when_i_click_the_deactivate_button(step):
    world.browser = TestApp(app)
    world.response = world.app.put('/api/v1.0/hotels/deactivate/{}/', world.id)


"""Update Hotel"""

@step(u'Given the new hotel id \'(.*)\' in database with the following details')
def given_the_new_hotel_id_in_database(step, hotel_id):
    world.hotel_oldInfo = step.hashes[0]

@step(u'When I update the hotel details into the following:')
def and_the_new_hotel_details_for_hotel_id(step):
    world.hotel_newInfo = step.hashes[0]
    world.browser = TestApp(app)
    world.hotel_url = '/api/v1.0/hotels/update/{}/'
    world.response = world.app.put(world.hotel_url, data = json.dumps(world.hotel_newInfo))


"""Add feature in feature_list"""


@step(u'Given I have the feature details:')
def given_i_have_the_following_hotel_features_details(step):
    world.features = step.hashes[0]


@step(u'When I click add button')
def when_i_click_the_add_button(step):
    world.browser = TestApp(app)
    world.feature_uri = '/api/v1.0/featureslist/'
    world.response = world.app.post(world.feature_uri, data =json.dumps(world.features))

@step(u'And it should have field "message" containing "Successfully Added"')
def successfully_added(step):
    resp = json.loads(world.response.data)
    assert_equals(resp['message'], 'Successfully Added')


"""Retrieve feature list"""

@step(u'Given hotel features id \'(.*)\'')
def given_hotel_features_id1_is_an_existing_feature_list(step, id_hotel_features_list):
    world.browser = TestApp(app)
    world.res_det = world.app.get('/api/v1.0/featureslists/{}/'.format(id_hotel_features_list))
    world.resp = json.loads(world.res_det.data)


@step(u'When I retrieve the hotel features \'(.*)\'')
def retrieve_feature_list(step, id):
    world.response = world.app.get('/api/v1.0/featureslists/{}/'.format(id))
    world.response = world.browser.get('/api/v1.0/featureslists/{}/', headers=world.headers)


@step(u'And the following are returned:')
def and_the_following_hotel_features_are_returned(step):
    assert_equals(step.hashes, [json.loads(world.response.data)])


"""update feature list"""


@step(u'Given feature with the following details:')
def given_a_hotel_feature_with_the_following_details(step):
    world.update = step.hashes[0]


@step(u'When I update the details into the following:')
def update_details(step):
    world.update1 = step.hashes[0]
    world.browser = TestApp(app)
    world.features_url = '/api/v1.0/featureslist/update/'
    world.response = world.app.put(world.features_url, data = json.dumps(world.update1))


"""Deactivate"""

@step(u'Given hotel feature id \'(.*)\' is a feature list')
def step_impl(step, id_hotel_features_list):
    world.id = id_hotel_features_list

@step(u'When I click the deactivate')
def click_deactivate(step):
    world.browser = TestApp(app)
    world.response = world.app.put('/api/v1.0/featureslist/deactivate/{}/', world.id)




"""adding system admin"""

@step("Given I already fill up the system admin signup form:")
def I_already_fill_up_the_system_admin_signup_form(step):
    world.details = step.hashes[0]

@step(u'When I submit the signup form')
def I_submit_the_signup_form(step):
    world.browser = TestApp(app)
    world.system_admin_url = '/api/v1.0/systemadmin/signup/'
    world.response = world.app.post(world.system_admin_url, data = json.dumps(world.details))


""" Retrieve admin"""


@step(u"Given an admin id \'(.*)\'")
def Given_an_admin_id_that_exists_with_the_following_details(step,id):
    world.browser = TestApp(app)
    world.sys_admin = world.app.get('/api/v1.0/systemadmin/{}/'.format(id))
    world.resp = json.loads(world.sys_admin.data)

@step(u'When I retrieve the admin id \'(.*)\'')
def When_I_retrieve_the_admin_id(step, id):
    world.response = world.app.get('/api/v1.0/systemadmin/{}/'.format(id))

@step(u"And the following system admin details are returned:")
def and_the_following_admin_details_are_returned(step):
    assert_equals(step.hashes, [json.loads(world.response.data)])


"""Update system admin details"""

@step(u'Given the new system admin id "1" in database with the following details')
def given_the_new_id_1_in_database(step):
    world.admin_oldInfo = step.hashes[0]

@step(u"When I update the system admin details into the following:")
def and_the_new_details_for_admin_id_1(step):
    world.admin_newInfo = step.hashes[0]
    world.browser = TestApp(app)
    world.admin_url = '/api/v1.0/systemadmin/update/'
    world.response = world.app.put(world.admin_url, data = json.dumps(world.admin_newInfo))



"""Search"""

@step(u'Given I filled up search box with keyword \'([^\']*)\'')
def given_i_filled_up_search_box_with_keyword_group1(step, group1):
    world.search_location = group1


@step(u'When I submit the keyword')
def when_i_submit_the_keyword(step):
    world.browser = TestApp(app)
    world.response = world.browser.get('/api/v1.0/search/{}/'.format(world.search_location))

@step(u'And it should return this following hotel with its detail:')
def and_it_should_return_this_following_hotel_with_its_detail(step):
    resp = json.loads(world.response.body)

    for column in step.hashes:
        world.id_hotel = column['id_hotel']
        world.hotel_name = column['hotel_name']
        world.description = column['description']
        world.email_address = column['email_address']
        world.address = column['address']
        world.contact_number = column['contact_number']
        world.no_of_restaurant = int(column['no_of_restaurant'])
        world.no_of_rooms = int(column['no_of_rooms'])
        world.extra = column['extra']

    col = [[{'id_hotel': int(world.id_hotel), 'hotel_name': str(world.hotel_name), 'description': str(world.description),
             'email_address': str(world.email_address), 'address': str(world.address),
             'contact_number': str(world.contact_number), 'no_of_restaurant': int(world.no_of_restaurant),
             'no_of_rooms': int(world.no_of_rooms), 'extra': str(world.extra)}]]

    assert_equals(col, [resp['entries']])



"""Add Hotel feature"""

@step(u'Given I have the following hotel features details:')
def given_i_have_the_following_hotel_features_details(step):
    world.hotelfeatures = step.hashes[0]


@step(u'When I add the hotel feature in the system')
def when_i_add_the_hotel_feature(step):
    world.browser = TestApp(app)
    world.hotelfeature = '/api/v1.0/hotelfeatures/'
    world.response = world.app.post(world.hotelfeature, data = json.dumps(world.hotelfeatures))


"""Retrieve hotel feature"""

@step(u'Given hotel features with id \'([^\']*)\'')
def given_hotel_features_id(step, id):
    world.browser = TestApp(app)
    world.res_det = world.app.get('/api/v1.0/hotelfeatures/{}/'.format(id))
    world.resp = json.loads(world.res_det.data)

@step(u'When I retrieve the hotel features with id \'([^\']*)\'')
def retrieve_hotel_features(step,id):
    world.response = world.app.get('/api/v1.0/hotelfeatures/{}/'.format(id))


@step(u'And the following hotel features are returned:')
def and_the_following_hotel_features_are_returned(step):
    assert_equals(step.hashes, [json.loads(world.response.data)])


"""Update hotel feature"""

@step(u'Given a hotel feature with the following details:')
def given_a_hotel_feature_with_the_following_details(step):
    world.update = step.hashes[0]


@step(u'When I update the details into the following:')
def update_details(step):
    world.update1 = step.hashes[0]
    world.browser = TestApp(app)
    world.hotelfeatures_url = '/api/v1.0/hotelfeatures/update/'
    world.response = world.app.put(world.hotelfeatures_url, data = json.dumps(world.update1))


"""Deactivate hotel feature"""


@step(u'Given hotel feature id \'(.*)\'')
def step_impl(step, id_hotel_features):
    world.id = id_hotel_features


@step(u'When I trigger the deactivate button')
def click_deactivate(step):
    world.browser = TestApp(app)
    world.response = world.app.put('/api/v1.0/hotelfeatures/deactivate/{}/', world.id)




""" add new image """


@step(u'Given i input the following details:')
def img_details(step):
    world.image = step.hashes[0]

@step(u'When i add the image in the system')
def add_image(step):
    world.browser = TestApp(app)
    world.img_url = '/api/v1.0/image/'
    world.response = world.app.post(world.img_url, data = json.dumps(world.image))



"""Retrieve image"""

@step(u'Given an image with id \'(.*)\'')
def idnumber_1_exist(step,id):
    world.browser = TestApp(app)
    world.det = world.app.get('/api/v1.0/image/{}/'.format(id))
    world.resp = json.loads(world.det.data)


@step(u'When i retrieve the image with id \'(.*)\'')
def when_i_retrieve_the_image(step,id):
    world.response = world.app.get('/api/v1.0/image/{}/'.format(id))


@step(u'Then the following details is returned:')
def following_info(step):
    world.ig = step.hashes[0]
    assert_equals(world.ig, json.loads(world.response.data))


"""Update Image"""

@step(u'Given an image exist with the following details:')
def given_the_following_details(step):
    world.data = step.hashes[0]


@step(u'When i change the image details to:')
def change_details(step):
    world.data1 = step.hashes[0]
    world.browser = TestApp(app)
    world.image_url = '/api/v1.0/image/'
    world.response = world.app.put(world.image_url, data = json.dumps(world.data))


"""Login"""


@step("login details")
def given_login_details(step):
    world.login = step.hashes[0]


@step("I click login button")
def when_i_click_login_button(step):
    world.browser = TestApp(app)
    world.response = world.app.post('/api/v1.0/login/', data = json.dumps(world.login))

@step("get a \'(.*)\' response")
def then_i_should_get_a_200_response(step, expected_status_code):
    assert_equals(world.response.status_code, int(expected_status_code))


@step('message "Successfully Logged In" is returned')
def message_res(step):
    world.respn = json.loads(world.response.data)
    assert_equals(world.respn['message'], "Successfully Logged In")