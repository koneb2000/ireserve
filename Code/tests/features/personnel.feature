Feature: Adding, Updating, and Retrieve hotel personnel details


	Scenario: Add hotel personnel
		Given I have the following hotel personnel details:
		  | fname | mname | lname | email | personnel_password | hotel_id |
		  | Neiell | amb | PAradiang | neiell@ymail.com | boo | 1 |
		When I click the register button
		Then I will get a '200' response
		And it should have a field "message" containing "OK" 


	Scenario: Add duplicate hotel personnel
		Given I have the following hotel personnel details:
		  | fname | mname | lname | email | personnel_password | hotel_id |
		  | Kristel | Ahlaine | Gem | pabillarankristel@ymail.com | asdasd | 1 |
		When I click the register button
		Then I will get a '200' response
		 And And it should have a field "message" containing "ID ALREADY EXISTS""


	Scenario: Add hotel personnel with empty email address field
		Given I have the following hotel personnel details:
		  | fname | mname | lname | email | personnel_password | hotel_id |
		  | Marj | buctolan | bermejo | | asdasd | 1 |
		When I click the register button
		Then I will get a '200' response
		And I should get a message containing 'Error'

	Scenario: Add hotel personnel with empty fname field
		Given I have the following hotel personnel details:
		  | fname | mname | lname | email | personnel_password | hotel_id |
		  |  | buctolan | bermejo | bermejo@gmail.com| asdasd | 1 |
		When I click the register button
		Then I will get a '200' response
		And I should get a message containing 'Error'


	Scenario: Add hotel personnel with empty mname field
		Given I have the following hotel personnel details:
		  | fname | mname | lname | email | personnel_password | hotel_id |
		  | Marj | | bermejo | bermejo | asdasd | 1 |
		When I click the register button
		Then I will get a '200' response
		And I should get a message containing 'Error'


	Scenario: Add hotel personnel with empty lname field
		Given I have the following hotel personnel details:
		  | fname | mname | lname | email | personnel_password | hotel_id |
		  | Marj | buctolan | | bermejo@gmail.com | asdasd | 1 |
		When I click the register button
		Then I will get a '200' response
		And I should get a message containing 'Error'


	Scenario: Add hotel personnel with empty personnel_password field
		Given I have the following hotel personnel details:
		  | fname | mname | lname | email | personnel_password | hotel_id |
		  | Marj | buctolan | bermejo | bermejo@gmail.com |  | 1 |
		When I click the register button
		Then I will get a '200' response
		And I should get a message containing 'Error'

	Scenario: Retrieving a personnel
		Given a hotel personnel id '6'
		When I retrieve hotel personnel '6'
		Then I will get a '200' response
		And the following hotel personnel details are returned:
		| id_personnel | fname | mname | lname | email | is_active | is_personnel | hotel_id |
		| 6 | Kristel | Ahlaine | Pabillaran | pabillarankristel@ymail.com | True | True | 1 |


	Scenario: Update hotel personnel details
		Given I have the following hotel personnel details:
		  | id_personnel | fname | mname | lname | email | personnel_password | hotel_id |
		  | 1 | Kristel | Ahlaine | Pabillaran | pabillarankristel@ymail.com | asdasd | 1 |
		When I update the personnel details into the following:
		  | id_personnel | fname | mname | lname | email | personnel_password | hotel_id |
		  | 1 | Kristel | Ahlaine | Gem | pabillarankristel@ymail.com | asdasd | 1 |
		Then I will get a '200' response
		And it should have a field "status" containing "success"


	Scenario: Deactivate hotel personnel
		Given hotel personnel id '1'
		When I click the deactivate button
		Then i will get a '200' response
		And it should have a field "status" containing "success"