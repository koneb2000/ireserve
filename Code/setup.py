from setuptools import setup

setup(name='iReserve',
      version='1.0',
      description='Reservation App',
      author='Neiell Care',
      author_email='team.ireserve@gmail.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['flask'],
     )
