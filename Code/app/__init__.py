
from flask import Flask, request, jsonify, session, make_response, g, send_from_directory
from flask_httpauth import HTTPTokenAuth
from itsdangerous import TimedJSONWebSignatureSerializer as JWT
from app.config import BaseConfig
import sys, os, flask, json
from functions import invalid, currencyConverter, payout
from models import DBconn, spcall
from flask.ext.cors import CORS, cross_origin
from flask.ext.bcrypt import Bcrypt
from werkzeug import secure_filename
# from flaskext.mail import Mail
from paypalrestsdk import Payout, ResourceNotFound
import paypalrestsdk
import requests
import ssl
import logging
import urllib2
import time


# config
app = Flask(__name__)
app.config.from_object(BaseConfig)
app.config['SECRET_KEY'] = 'EERwyDyEfWWO4NLFAqs8m4UZxKhZvMOsgeKqi1G0jgyREwE4LuZLC_g677uCJXcHUP7013FU65yAGoHM'

jwt = JWT(app.config['SECRET_KEY'], expires_in=3600)
UPLOAD_FOLDER = 'images/'
basedir = os.path.abspath(os.path.dirname(UPLOAD_FOLDER))

app.config['UPLOAD_FOLDER'] = basedir
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])


auth = HTTPTokenAuth('Bearer')
CORS(app, supports_credentials=True)
#mail = Mail(app)

@auth.verify_token
def verify_token(token):
    g.user = None
    try:
        data = jwt.loads(token)
    except:
        return False
    if 'user' in data:
        g.user = data['user']
        g.id = data['id']
        return True
    return False


# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


"""routes"""

@app.route('/api/v1.0/login/', methods=['POST'])
def loginhoteladmin():
    data = request.json
    if invalid(data['email']):
        status = False
        return jsonify({'status': status, 'message': 'Invalid Email address'})

    res = spcall("loginauth", (data['email'], data['password']))

    if res[0][0] == 'ERROR':
        status = False
        return jsonify({'status': status, 'message': 'error'})
    else:
        status = True
        token = jwt.dumps({'user': data['email'], 'id': res})
        return jsonify({'status': status, 'token': token, 'hotel_id': res, 'login_type': 1, 'message': 'success'})


@app.route('/api/v1.0/personnel/login/', methods=['POST'])
def loginpersonnel():
    data = request.json
    if invalid(data['email']):
        status = False
        return jsonify({'status': status, 'message': 'Invalid Email address'})

    res = spcall("loginpersonnelauth", (data['email'], data['password']))

    if res[0][0] == 'ERROR':
        status = False
        return jsonify({'status': status, 'message': 'error'})
    else:
        status = True
        token = jwt.dumps({'user': data['email'], 'id': res})
        return jsonify({'status': status, 'token': token, 'hotel_id': res, 'login_type': 0,'message': 'success'})



@app.route('/api/v1.0/loginAdmin/', methods=['POST'])
def loginsystemadmin():
    data = request.json
    if invalid(data['email']):
        return jsonify({'status': 'error', 'message': 'Invalid Email address'})

    res = spcall("login_system_admin", (data['email'], data['password']))
    if res == 'ERROR':
        return jsonify({'status': 'OK', 'message': 'error'})

    token = jwt.dumps({'user': data['email'], 'id': res})
    return jsonify({'status': 'OK', 'token': token, 'hotel_id': res, 'login_type': 2})


""" HOTEL """

@app.route('/api/v1.0/hotels/signup/', methods=['POST'])
def hotel_signup():
    jsn = json.loads(request.data)

    if invalid(jsn['email_address']):
        return jsonify({'status': 'error', 'message': 'Invalid Email address'})

    res = spcall("newhotel", (
        jsn['email_address'], jsn['hotel_name'], jsn['website_url'], jsn['description'],
        jsn['paypal_account'], jsn['password'], jsn['address'], jsn['contact_number'],
        int(jsn['no_of_rooms']), ' '), True)

    if 'error' in res[0][0]:
        return jsonify({'status': 'error', 'message': res[0][0]})

    return jsonify({'status': 'OK', 'message': res[0][0]})

@app.route('/api/v1.0/hotels/<hotel_id>/', methods=['GET'])
def gethotel(hotel_id):
    res = spcall('gethotel_id', [hotel_id])
    rooms = spcall('getid_room', [hotel_id])
    hotel_features = spcall('getid_features', [hotel_id])
    images = spcall('getimage_id', [int(hotel_id)])

    img = []
    for r in images:
        img.append(
         r[1]
         )

    if not res:
        return jsonify({'status': 'Error', 'message': 'Results Not Found'}), 404

    rec = res[0]

    return jsonify({"id": hotel_id,"email_address": str(rec[0]), "hotel_name": str(rec[1]),
                    "website_url": str(rec[2][8:]), "description": str(rec[3]),
                    "paypal_account": str(rec[4]), "address": str(rec[5]), 
                    "contact": str(rec[6]), "no_of_rooms": str(rec[7]),
                    "extra": str(rec[8]), "is_verified": str(rec[9]),
                    "is_hotel_admin": str(rec[10]), "is_active": str(rec[11]),
                    "rooms": rooms, "hotel_features":hotel_features, 'images': img})


@app.route('/api/v1.0/hotels/', methods=['GET'])
def get_all_hotels():
    res = spcall('gethotel', ())

    if 'Error' in str(res[0][0]):
        return jsonify({'status': 'error', 'message': res[0][0]})

    recs = []
    for r in res:
        recs.append({"hotel_id": str(r[0]), "email_address": str(r[1]), "hotel_name": str(r[2]), "website_url": str(r[3]), "description": str(r[4]),
                    "paypal_account": str(r[5]), "password": str(r[6]), "address": str(r[7]), "contact_number": str(r[8]),
                    "map_latitude": str(r[9]), "map_longitude": str(r[10]), "no_of_rooms": str(r[11]), "extra": str(r[12]),
                    "is_verified": str(r[13]), "is_hotel_admin": str(r[14]), "is_active": str(r[15])})

    return jsonify({'status': 'ok', 'entries': recs, 'count': len(recs)})

@app.route('/api/v1.0/hotels/deactivate/<hotel_id>/', methods=['PUT'])
@auth.login_required
def deactivateHotel(hotel_id):
    spcall('deactivate_hotel',
        hotel_id, True)

    return jsonify({'status': 'success'})

@app.route('/api/v1.0/hotels/update/<hotel_id>/', methods = ['PUT'])
@auth.login_required
def update_hotel(hotel_id):

    jsn = json.loads(request.data)
    email_address = jsn.get('email_address', ''),
    hotel_name = jsn.get('hotel_name', ''),
    website_url = jsn.get('website_url', ''),
    description = jsn.get('description', ''),
    paypal_account = jsn.get('paypal_account', ''),
    password = jsn.get('password', ''),
    address = jsn.get('address', '')
    contact_number = jsn.get('contact_number', ''),
    no_of_rooms = jsn.get('no_of_rooms', ''),
    extra = jsn.get('extra', ''),
    is_verified = False,
    is_hotel_admin = True,
    is_active = True

    response = spcall('update_hotel',(
        email_address,
        hotel_name,
        website_url,
        description,
        paypal_account,
        password,
        address,
        contact_number,
        no_of_rooms,
        extra,
        is_verified,
        is_hotel_admin,
        is_active), True)

    return jsonify({'status': 'OK'})


""" END of HOTEL """


"""Hotel  Personnel"""


@app.route('/api/v1.0/hotelpersonnels/<id_personnel>/', methods=['GET'])
@auth.login_required
def get_personnel(id_personnel):
    res = spcall('getid_personnel', [id_personnel])

    if 'Error' in str(res[0][0]):
        return jsonify({'status': 'error', 'message': res[0][0]})

    rec = res[0]
    return jsonify({"id_personnel": str(id_personnel), "email": str(rec[0]), "fname": str(rec[1]), "mname": str(rec[2]), "lname": str(rec[3]),
                        "is_active": str(rec[5]), "is_personnel": str(rec[6]), "hotel_id": str(rec[7])})

@app.route('/api/v1.0/hotelpersonnels/', methods=['GET'])
@auth.login_required
def get_all_personnel():
    res = spcall('getHotel_Personnel', ())

    if 'Error' in str(res[0][0]):
        return jsonify({'status': 'error', 'message': res[0][0]})

    recs = []
    for r in res:
        recs.append({"id_personnel": str(r[0]), "email": str(r[1]), "fname": str(r[2]), "mname": str(r[3]), "lname": str(r[4]),
                     "is_active": str(r[6]), "is_personnel": str(r[7]), "hotel_id": str(r[8])})

    return jsonify({'status': 'ok', 'entries': recs, 'count': len(recs)})
    
@app.route('/api/v1.0/hotelpersonnel/', methods=['POST'])
@auth.login_required
def new_personnel():
    jsn= json.loads(request.data)

    if invalid(jsn['email']):
        return jsonify({'status': 'error', 'message': 'Error'})

    res = spcall("newhotel_personnel", (
        jsn['fname'],
        jsn['mname'],
        jsn['lname'],
        jsn['email'],
        jsn['personnel_password'],
        jsn['hotel_id']
    ), True)

    if 'Error' in res[0][0]:
        return jsonify({'status': 'ok', 'message': res[0][0]})

    return jsonify({'status': 'ok', 'message': res[0][0]})


@app.route('/api/v1.0/hotelpersonnel/update/', methods=['PUT'])
@auth.login_required
def update():
    jsn = json.loads(request.data)

    id_personnel = jsn.get('id_personnel', '')
    fname = jsn.get('fname', '')
    mname = jsn.get('mname', '')
    lname = jsn.get('lname', '')
    email = jsn.get('email', '')
    personnel_password = jsn.get('personnel_password', '')

    spcall('updatePersonnel', (
        id_personnel,
        fname,
        mname,
        lname,
        email,
        personnel_password
    ), True)

    return jsonify({'status': 'success'})


@app.route('/api/v1.0/hotelpersonnel/deactivate/<id>/', methods=['PUT'])
@auth.login_required
def deactivatePersonnel(id):
    spcall('deactivate',
        id, True)

    return jsonify({'status': 'success'})


"""Room"""

@app.route('/api/v1.0/room/', methods=['POST'])
@auth.login_required
def add_room():
    req = json.loads(request.data)

    res = spcall("newroom", (
        req['room_type'],
        req['cost'],
        req['max_person'],
        req['total_room'],
        req['hotel_id']
        ))


    if 'Error' in res[0][0]:
        return jsonify({'status': 'error', 'message': res[0][0]})

    return jsonify({'status': 'ok', 'message': res[0][0]})


@app.route('/api/v1.0/room/<id>/', methods=['GET'])
def get_roomid(id):

    res = spcall('getid_room', [id])

    if not res:
        return jsonify({'status': 'error', 'message': 'Results Not Found'}),404

    recs = res[0]

    return jsonify({'room_type' : str(recs[0]), 'cost': str(recs[1]), 'max_person': str(recs[2]), 'total_room': str(recs[3]), 'hotel_id': str(recs[4])})


@app.route('/api/v1.0/room/', methods=['GET'])
def get_rooms():

    res = spcall('getroom', ())

    if 'Error' in str(res[0][0]):
        return jsonify({'status': 'error', 'message': res[0][0]})

    recs = []
    for r in res:
        recs.append({
         "id": int(r[0]),
         "room_type": r[1],
         "cost": (r[2]),
         "max_person": str(r[3]),
         "total_room": str(r[4]),
         "hotel_id": str(r[5])
         })

    return jsonify({'status': 'ok', 'entries': recs, 'count': len(recs)})


@app.route('/api/v1.0/room/', methods=['PUT'])
@auth.login_required
def updateroom():
    jsn = json.loads(request.data)

    id_room = jsn.get('id_room','')
    room_type = jsn.get('room_type', '')
    cost = jsn.get('cost', '')
    max_person = jsn.get('max_room', '')
    total_room = jsn.get('total_room', '')

    res = spcall('updateroom', (
        id_room,
        room_type,
        cost,
        max_person,
        total_room),True)


    if 'Error' in str(res[0][0]):
        return jsonify({'status': 'error', 'message': res[0][0]})

    return jsonify({"status": "success"})





@app.route('/api/v1.0/feedback/', methods=['POST'])
def feedback():
    """             Creating a Feedback                   """

    res = spcall("newfeedback", (request.form['name'], request.form['comment'], request.form['hotel_id']))

    if 'Error' in res[0][0]:
        return jsonify({'status': 'error', 'message': res[0][0]})

    return jsonify({'status': 'ok', 'message': res[0][0]})


@app.route('/api/v1.0/feedback/<id>/',methods=['GET'])
def view_feedback(id):

    """             Viewing Specific Feedback                """

    res = spcall("getfeedback", id, True)

    recs = []
    for r in res:
        recs.append({"id": int(r[0]), "name": r[1], "comment": r[2], "created_date": r[3]})
    return jsonify({'status': 'ok', 'entries': recs, 'count': len(recs)})

@app.route('/api/v1.0/search/', methods=['POST'])
def search():
    """             Searching a hotel through its location                   """
    data = request.json


    res = spcall("location_search", [data['location']])

    
    recs = []

    for r in res:
        image = []
        img = spcall("getimage_id", [int(r[0])])
        image.append(img)

        recs.append({"id_hotel": int(r[0]), "hotel_name": r[1], "website_url": r[2], "description": r[3], "email_address": r[4], "paypal_account": r[5],
         "address": r[6], "contact_number": r[7], "no_of_rooms": r[8], "extra": r[9], "id_room": r[10], "room_name": r[11], "cost": r[12], "max_person": r[13],
         "available_room": r[14], "total_room": r[15], "image": image})

    return jsonify({'status': 'ok', 'entries': recs, 'count': len(recs)})


"""IMAGE"""


@app.route('/api/v1.0/upload/image/', methods=['POST'])
@auth.login_required
def upload_file():
    uploaded_files = request.files.getlist("file[]")
    filenames = []
    for file in uploaded_files:
        # Check if the file is one of the allowed types/extensions
        if file and allowed_file(file.filename):
            # Make the filename safe, remove unsupported chars
            filename = secure_filename(file.filename)
            # Move the file form the temporal folder to the upload
            # folder we setup
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            # Save the filename into a list, we'll use it later
        else:
            return jsonify({'status': 'error'})
        hotel_id = g.id[0][0]
        res = spcall('newimage', (filename, int(hotel_id)), True)
    return jsonify({'status': 'OK'})



#Hotel Features
@app.route('/api/v1.0/hotelfeatures/', methods=['POST'])
@auth.login_required
def new_hotelfeatures():
    dat = json.loads(request.data)
    res = spcall("newHotel_Features", (
        dat['name'],
        int(dat['hotel_id'])
    ))

    if 'Error' in res[0][0]:
        return jsonify({'message': res[0][0]})

    return jsonify({'status': 'ok', 'message': res[0][0]})



@app.route('/api/v1.0/image/<id>/', methods = ['GET'])
@auth.login_required
def getimage(id):
    res = spcall('getimage_id', [int(id)])

    if not res:
        return jsonify({'status': 'error', 'message': 'Results Not Found'}),404
    recs = []
    for r in res:
        recs.append({
         "name": r[1]
         })

    return jsonify({'img': recs, 'count': len(recs)})


@app.route('/uploads/<filename>')
@auth.login_required
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],filename)



"""business information"""

@app.route('/api/v1.0/business_info/', methods = ['POST'])
@auth.login_required
def addbusinessinfo():

    jsn = json.loads(request.data)

    res = spcall('newbusinessinfo', (
        jsn['business_license'],
        jsn['tax_identification_number'],
        jsn['proof_pic'],
        jsn['hotel_id']), True)

    if 'Error' in res[0][0]:
        return jsonify({'status': 'ok', 'message': res[0][0]})

    return jsonify({'status': 'OK', 'message': 'success'})


@app.route('/api/v1.0/business_infos/', methods = ['GET'])
@auth.login_required
def getbusinessinfos():

    res = spcall('getbusinessinfo', ())

    if 'Error' in str(res[0][0]):
        return jsonify({'status': 'ok', 'message': res[0][0]})

    recs = []
    for r in res:
        recs.append({"hotel_name": str(r[0]), "business_license": str(r[1]), "tax_identification_number": str(r[2]),
                     "proof_picture": str(r[3]), "hotel_id": str(r[4])})

    return jsonify({'status': 'ok', 'entries': recs, 'count': len(recs)})


@app.route('/api/v1.0/business_infos/<id>/', methods = ['GET'])
@auth.login_required
def business_id(id):

    res = spcall('getbusinessinfo_id',[id])

    if not res:
        return jsonify({'status': 'error', 'message': 'Results Not Found'}),404


    recs = res[0]

    return jsonify({'business_license': str(recs[0]), 'tax_identification_number': str(recs[1]), 'proof_pic': str(recs[2]), 'hotel_id': str(recs[3])})


@app.route('/api/v1.0/business_info/', methods = ['PUT'])
@auth.login_required
def updatebusinessinfo():
    jsn = json.loads(request.data)

    id_info= jsn.get('id_info','')
    business_license = jsn.get('business_license', '')
    tax_identification_number = jsn.get('tax_identification_number', '')
    proof_pic = jsn.get('proof_pic', '')


    res = spcall('updatebusinessinfo', (
        id_info,
        business_license,
        tax_identification_number,
        proof_pic
        ),True)

    return jsonify({"status": "success"})


    if 'Error' in str(res[0][0]):
        return jsonify({'status': 'error', 'message': res[0][0]})

"""Hotel Features"""

@app.route('/api/v1.0/hotelfeatures/', methods=['GET'])
@auth.login_required
def get_all_hotelfeatures():
    res = spcall('getHotel_Features', ())


    if 'Error' in str(res[0][0]):
        return jsonify({'status': 'error', 'message': res[0][0]})

    recs = []
    for r in res:
        recs.append({
         "id_image": int(r[0]),
         "img": r[1],
         "hotel_id": (r[2]),
         "room_id": str(r[3])
         })
        recs.append({"id_hotel_features": str(r[0]), "name": str(r[1]), "hotel_id": str(r[2]), "is_active": str(r[3])})


    return jsonify({'status': 'ok', 'entries': recs, 'count': len(recs)})


@app.route('/api/v1.0/hotelfeatures/<id_hotel_features>/', methods=['GET'])
@auth.login_required
def get_hotelfeature(id_hotel_features):
    res = spcall('getid_features', ([id_hotel_features]))

    if 'Error' in res[0][0]:
        return jsonify({'status': 'error', 'message': res[0][0]})

    recs = []
    for r in res:
        recs.append({"id_feature": r[0], "name": r[1]})
    return jsonify({"entries": recs})


@app.route('/api/v1.0/hotelfeatures/update/', methods=['PUT'])
@auth.login_required
def updatehotelfeature():
    jsn = json.loads(request.data)

    id_hotel_features = jsn.get('id_hotel_features', '')
    name = jsn.get('name', '')
    hotel_id = jsn.get('hotel_id', '')

    spcall('updateHotel_Features', (
        id_hotel_features,
        name,
        hotel_id
    ), True)

    return jsonify({'status': 'success'})


@app.route('/api/v1.0/hotelfeatures/deactivate/<id_hotel_features>/', methods=['PUT'])
@auth.login_required
def deactivateFeature(id_hotel_features):
    spcall('deactivate_hotelfeature', id_hotel_features, True)

    return jsonify({'status': 'success'})


"""Feature List"""

@app.route('/api/v1.0/featureslist/', methods=['POST'])
@auth.login_required
def new_features():
    dat = json.loads(request.data)

    res = spcall("newfeature_list", (
        dat['name'],
        int(dat['hotel_features'])
    ))

    if 'Error' in res[0][0]:
        return jsonify({'message': res[0][0]})

    return jsonify({'status': 'ok', 'message': res[0][0]})


@app.route('/api/v1.0/featureslists/', methods=['GET'])
@auth.login_required
def get_all_features():
    res = spcall('getfeatures_list', ())

    if 'Error' in str(res[0][0]):
        return jsonify({'status': 'error', 'message': res[0][0]})

    recs = []
    for r in res:
        recs.append({"id_hotel_features_list": str(r[0]), "name": str(r[1]), "hotel_features": str(r[2]), "is_active": str(r[3])})

    return jsonify({'status': 'ok', 'entries': recs, 'count': len(recs)})


@app.route('/api/v1.0/featureslists/<id_hotel_features_list>/', methods=['GET'])
@auth.login_required
def get_feature(id_hotel_features_list):
    res = spcall('getid_featureslist', [int(id_hotel_features_list)])

    if 'Error' in str(res[0][0]):
        return jsonify({'status': 'error', 'message': res[0][0]})

    rec = res[0]
    return jsonify({"id_hotel_features_list": str(id_hotel_features_list), "name": str(rec[0]), "hotel_features": str(rec[1]), "is_active": str(rec[2])})


@app.route('/api/v1.0/featureslist/update/', methods=['PUT'])
@auth.login_required
def updatefeature():
    jsn = json.loads(request.data)

    id_hotel_features_list = jsn.get('id_hotel_features_list', '')
    name = jsn.get('name', '')
    hotel_features = jsn.get('hotel_features', '')

    spcall('updateFeatures_List', (
        id_hotel_features_list,
        name,
        hotel_features
    ), True)

    return jsonify({'status': 'success'})


@app.route('/api/v1.0/featureslist/deactivate/<id_hotel_features_list>/', methods=['PUT'])
@auth.login_required
def deactivateFeaturelist(id_hotel_features_list):
    spcall('deactivate_hotelfeature', id_hotel_features_list, True)

    return jsonify({'status': 'success'})


""" System Admin """

@app.route('/api/v1.0/systemadmin/signup/', methods=['POST'])
def signup_system_admin():
    req = json.loads(request.data)

    res = spcall("newSystem_admin",(
        req['email_address'],
        req['fname'],
        req['lname'],
        req['password']
        ), True)

    if 'Error' in res[0][0]:
        return jsonify({'status': 'ID EXISTED', 'message': res[0][0]})

    return jsonify({'status': 'OK', 'message': res[0][0]})


@app.route('/api/v1.0/systemadmin/<id_admin>/', methods=['GET'])
def getadmin_id(id_admin):
    res = spcall('getAdmin_id', id_admin)

    rec = res[0]
    return jsonify({"id_admin": str(id_admin), "email_address": str(rec[0]), "fname": str(rec[1]), "lname": str(rec[2]),
                    "is_system_admin": str(rec[4])})


@app.route('/api/v1.0/systemadmin/getalladmins/', methods=['GET'])
def get_all_admins():
    res = spcall('getSystemAdmin', ())

    if 'Error' in str(res[0][0]):
        return jsonify({'status': 'error', 'message': res[0][0]})

    rec = []
    for r in res:
        rec.append({"id_admin": str(r[0]), "email_address": str(rec[1]), "fname": str(rec[2]), "lname": str(rec[3]),
                    "is_system_admin": str(rec[5])})

    return jsonify({'status': 'ok', 'entries': rec, 'count': len(rec)})


@app.route('/api/v1.0/systemadmin/update/', methods=['PUT'])
def update_system_admin():
    req = json.loads(request.data)

    fname=req.get('fname', ''),
    lname=req.get('lname', ''),
    email_address=req.get('email_address', ''),
    password=req.get('password', '')

    res = spcall('updateSystemAdmin', (
        fname,
        lname,
        email_address,
        password), True)
    return jsonify({'status': 'OK'})


"""Paypal Transaction"""
"""Paypal Transaction"""

@app.route('/api/v1.0/hotel/transaction/', methods=['POST'])
def transaction():
    data = request.json
    fee = int(currencyConverter('PHP', 'USD', int(data['cost'])))

    payment = paypalrestsdk.Payment({
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"},
        "redirect_urls": {
            "return_url": "http://localhost:8080/Code/app/static/#/afterpayment",
            "cancel_url": "https://devtools-paypal.com/guide/pay_paypal/python?cancel=true"},

        "transactions": [{
            "amount": {
                "total": fee,
                "currency": "USD"},
            "description": "Reservation Payment"}]})

    payment.create()
    for link in payment.links:
        if link.method == "REDIRECT":
            redirect_url = link.href

    res = spcall("newtransaction", (payment.id, time.strftime("%d-%m-%Y") , time.strftime("%d-%m-%Y"),
            int(data['cost']), int(data['hotel_id']), int(data['room_id'])), True)
    if res[0][0] == 'OK':
        return jsonify(
            {'id': payment.id, 'redirect': redirect_url, 'message': res[0][0]})

    return jsonify(
        {'message': 'Error'})


@app.route('/api/v1.0/hotel/transaction/execute', methods=['PUT'])
def execute_transaction():
    # payer_id = request.args.get('PayerID')
    # payment_id = request.args.get('paymentId')
    # token = request.args.get('token')
    data = request.json
    try:
        payment = paypalrestsdk.Payment.find(data['payment_id'])
    except paypalrestsdk.exceptions.ResourceNotFound as ex:
        ('Paypal resource not found: {}'.format(ex))
        abort(404)
    if payment:
        spcall("update_transaction", (str(data['payment_id']), str(data['payer_id']), str(data['token']), str(data['name']), str(data['email']), data['contact_number']), True)
        return jsonify({'status': 'ok'})
    else:
        return jsonify({'status': 'Error'})
    

@app.route('/api/v1.0/hotelpersonnel/<hotel_id>/transaction/view/', methods=['GET'])
@auth.login_required
def view_transaction(hotel_id):
    res = spcall("gettransaction", (hotel_id))

    recs = []
    for r in res:
        recs.append({'id': r[0], 'transaction_number': r[1], 'payer_id': r[2], 'paypal_token': r[3], 'payment_id': r[4],
                     'name': r[5], 'contact_number': r[6], 'email_address': r[7], 'checkin': str(r[8]),'checkout': str(r[9]),
                     'room_id': r[10], 'fee': r[11], 'confirmed': r[12], 'is_done': r[13]})

    return jsonify({'status': 'ok', 'entries': recs, 'count': len(recs)})


@app.route('/api/v1.0/hotel/transaction/confirm/', methods=['POST'])
@auth.login_required
def confirm_transaction():
    """

    """

    data = request.json
    try:
        payment = paypalrestsdk.Payment.find(data['payment_id'])
    except paypalrestsdk.exceptions.ResourceNotFound as ex:
        res = 'Paypal resource not found: {}'.format(ex)
        return jsonify({'message': res})

    if payment.execute({"payer_id": data['payer_id']}):
        spcall("confirm_transaction", ([data['payment_id']]), True)
        getHotel = spcall('gethotel_id', [data['id']])
        res = payout(data['transaction_number'], getHotel[0][4], data['fee'], data['transaction_id'])
        return jsonify({'status': 'approved', 'payout': res})

    return jsonify({'status': 'error'})


@app.route('/api/v1.0/verify_hotel', methods = ['PUT'])
@auth.login_required
def verify(id):
    res = spcall('verifyaccount', [id], True)

    return jsonify({'status': 'ok', 'message' : 'Success'})


@app.after_request
def add_cors(resp):
    resp.headers['Access-Control-Allow-Origin'] = flask.request.headers.get('Origin', '*')
    resp.headers['Access-Control-Allow-Credentials'] = True
    resp.headers['Bearer'] = True
    resp.headers['Bearer'] = 'POST, OPTIONS, GET, PUT, DELETE'
    resp.headers['Access-Control-Allow-Methods'] = 'POST, OPTIONS, GET, PUT, DELETE'
    resp.headers['Access-Control-Allow-Headers'] = flask.request.headers.get('Access-Control-Request-Headers',
                                                                             'Authorization')
    # set low for debugging
    if app.debug:
        resp.headers["Access-Control-Max-Age"] = '1'
    return resp