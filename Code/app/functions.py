from werkzeug import secure_filename
import paypalrestsdk
from paypalrestsdk import Payout, ResourceNotFound
import requests
import ssl
import logging
import urllib2
import time
import json
import flask


GENERIC_DOMAINS = "aero", "asia", "biz", "cat", "com", "coop", \
                  "edu", "gov", "info", "int", "jobs", "mil", "mobi", "museum", \
                  "name", "net", "org", "pro", "tel", "travel"


def invalid(emailaddress, domains=GENERIC_DOMAINS):
    """Checks for a syntactically invalid email address."""

    # Email address must be 7 characters in total.
    if len(emailaddress) < 7:
        return True  # Address too short.

    # Split up email address into parts.
    try:
        localpart, domainname = emailaddress.rsplit('@', 1)
        host, toplevel = domainname.rsplit('.', 1)
    except ValueError:
        return True  # Address does not have enough parts.

    # Check for Country code or Generic Domain.
    if len(toplevel) != 2 and toplevel not in domains:
        return True  # Not a domain name.

    for i in '-_.%+.':
        localpart = localpart.replace(i, "")
    for i in '-_.':
        host = host.replace(i, "")

    if localpart.isalnum() and host.isalnum():
        return False  # Email address is fine.
    else:
        return True  # Email address has funny characters.

"""PAYPAL"""

def currencyConverter(currency_from, currency_to, currency_input):
    """

        Real-time Currency Converter using yahoo services

    """

    yql_base_url = "https://query.yahooapis.com/v1/public/yql"
    yql_query = 'select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20("' + currency_from + currency_to + '")'
    yql_query_url = yql_base_url + "?q=" + yql_query + "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys"
    try:
        yql_response = urllib2.urlopen(yql_query_url)
        try:
            yql_json = json.loads(yql_response.read())
            currency_output = currency_input * float(yql_json['query']['results']['rate']['Rate'])
            return currency_output
        except (ValueError, KeyError, TypeError):
            return "JSON format error"

    except IOError, e:
        if hasattr(e, 'code'):
            return e.code
        elif hasattr(e, 'reason'):
            return e.reason


def payout(transaction_number, email, fee, transaction_id):
    """

        Paypal Payout
        Send from iReserve Account to Hotel

    """
    payout = Payout({
        "sender_batch_header": {
            "sender_batch_id": transaction_number,
            "email_subject": "Reservation Payment"
        },
        "items": [
            {
                "recipient_type": "EMAIL",
                "amount": {
                    "value": int(currencyConverter('PHP', 'USD', fee)),
                    "currency": "USD"
                },
                "receiver": email,
                "note": "Reservation Payment",
                "sender_item_id": transaction_id
            }
        ]
    })

    if payout.create(sync_mode=True):
        return "payout[%s] created successfully" % (payout.batch_header.payout_batch_id)
    else:
        return payout.error
