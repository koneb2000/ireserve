var myApp = angular.module('myApp', ['ui.router']);

myApp.config(function ($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise("/404");

  $stateProvider
      
      .state('landingpage', {
          url: "",
          templateUrl: "partials/landingpage.html",
          controller: 'searchController'
      })
      .state('404page', {
          url: "/404",
          templateUrl: "partials/404.html",
      })
      .state('hotellogin', {
          url: "/hotellogin",
          templateUrl: 'partials/loginhotel.html',
          controller: 'userController'
      })
      .state('personnelogin', {
          url: "/loginpersonnel",
          templateUrl: 'partials/loginpersonnel.html',
          controller: 'userController'
      })
      .state('systemadminlogin', {
          url: "/loginadmin",
          templateUrl: 'partials/loginadmin.html',
          controller: 'userController'
      })
      .state('registerHotel', {
          url: '/register',
          templateUrl: 'partials/register.html',
          controller: 'userController'
      })
      .state('logout', {
          url: '/logout',
          templateUrl: 'partials/logout.html',
          controller: 'logoutController'
      })
      .state('search', {
          url: '/search',
          templateUrl: 'partials/search.html',
          controller: 'searchController'
      })
      .state('profile', {
          url: '/profile',
          templateUrl: 'partials/profile.html',
          controller: 'searchController'
      })
      .state('about', {
          url: '/about',
          templateUrl: 'partials/about.html'
      })
      .state('afterpayment', {
          url: '/afterpayment',
          templateUrl: 'partials/afterpayment.html',
          controller: 'transactionController'
      })
      .state('forgotpassword', {
          url: '/forgotpassword',
          templateUrl: 'partials/forgotpassword.html'
      })
      .state('system_admin',{
          url:'/admin',
          templateUrl:'partials/system_admin/header.html',
          controller: 'systemAdminController'
      })
      .state('system_admin_editprofile',{
        url:'/admin/edit_profile',
        templateUrl:'partials/system_admin/edit_profile.html',
        controller: 'systemAdminController'
    })
      .state('system_admin_reportedaccounts',{
          url:'/admin/reportedaccounts',
          templateUrl:'partials/system_admin/reported_accounts.html',
          controller: 'systemAdminController'
      })
      .state('system_admin_reportedfeedbacks',{
          url:'/admin/reportedfeedbacks',
          templateUrl:'partials/system_admin/reported_feedbacks.html',
          controller: 'systemAdminController'
      })
     .state('system_admin_verifyaccount',{
        url:'/admin/verifyaccount',
        templateUrl:'partials/system_admin/verified_account.html',
        controller: 'systemAdminController'
    })
       .state('system_admin_viewinfo',{
        url:'/admin/viewinfo',
        templateUrl:'partials/system_admin/viewinfo.html',
        controller: 'systemAdminController'
    })
.state('hotel_admin',{
          url:'/hoteladmin',
          templateUrl:'partials/hotel_admin/home.html',
          controller: 'hotelAdminController'
      })
      .state('hotel_admin_addpersonnel',{
          url:'/hoteladmin/addpersonnel',
          templateUrl:'partials/hotel_admin/addhotelpersonnel.html',
          controller: 'hotelAdminController'
      })
      .state('hotel_admin_viewhotelpersonnel',{
          url:'/hoteladmin/viewpersonnel',
          templateUrl:'partials/hotel_admin/viewpersonnel.html',
          controller: 'hotelAdminController'
      })
      .state('hotel_admin_updateprofile',{
          url:'/hoteladmin/updateprofile',
          templateUrl:'partials/hotel_admin/updateprofile.html',
          controller: 'hotelAdminController'
      })
      .state('hotel_admin_deletepersonnel',{
          url:'/hoteladmin/deletepersonnel',
          templateUrl:'partials/hotel_admin/deletepersonnel.html',
          controller: 'hotelAdminController'
      })
      .state('hotel_admin_hotelprofile',{
          url:'/hoteladmin/hotelprofile',
          templateUrl:'partials/hotel_admin/hotelprofile.html',
          controller: 'hotelAdminController'
      })
      .state('hotel_admin_validation',{
          url:'/hoteladmin/validation',
          templateUrl:'partials/hotel_admin/validation.html',
          controller: 'hotelAdminController'
      })
      .state('hotel_admin_view_room',{
          url:'/hoteladmin/viewrooms',
          templateUrl:'partials/hotel_admin/viewrooms.html',
          controller: 'hotelAdminController'
      })
      .state('hotel_admin_update_room',{
          url:'/hoteladmin/updaterooms',
          templateUrl:'partials/hotel_admin/updaterooms.html',
          controller: 'hotelAdminController'
      })
      .state('hotel_admin_add_room',{
          url:'/hoteladmin/add_room',
          templateUrl:'partials/hotel_admin/add_room.html',
          controller: 'hotelAdminController'
      })
      .state('hotel_admin_image',{
          url:'/hoteladmin/image',
          templateUrl:'partials/hotel_admin/image.html',
          controller: 'imageController'
      })
      .state('hotel_personnel',{
          url:'/hotelpersonnel',
          templateUrl:'partials/personnel/home.html',
          controller: 'hotelPersonnelController'
      })
      .state('hotel_personnel_editprofile',{
          url:'/hotel_personnel/editprofile',
          templateUrl:'partials/personnel/editprofile.html',
          controller: 'hotelPersonnelController'
      })
      .state('hotel_personnel_profile',{
          url:'/hotel_personnel/profile',
          templateUrl:'partials/personnel/profile.html',
          controller: 'hotelPersonnelController'
      })
      .state('systemadmin',{
          url:'/systemAdmin2',
          views: {
              "sidebar": {
                  templateUrl: 'admins/partials/systemadmin_sidebar'
              }
          }
      })
      .state('hotel_personnel_transaction',{
          url:'/hotelpersonnel/transaction',
          templateUrl:'partials/personnel/transaction.html',
          controller: 'hotelPersonnelController'
      });
});


myApp.run(['$window', '$rootScope', 
function ($window ,  $rootScope) {
  $rootScope.goBack = function(){
    $window.history.back();
  }
}]);
/*

myApp.run(function ($rootScope) {

  $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
    var requireLogin = toState.data.requireLogin;
    if (requireLogin && typeof $rootScope.currentUser === 'undefined') {
      event.preventDefault();
      // get me a login modal!
    }
  });

});*/


// template: '<li>' +
// '<a href="home.html" class=" hvr-bounce-to-right">' + '<i class="fa fa-dashboard nav_icon ">' + '</i><span class="nav-label"></span>Dashboard</a>' +
// '</li>' +
// '<li>' + '<a href="reported_accounts.html" class=" hvr-bounce-to-right">' + '<i class="fa fa-list-alt nav_icon "></i>' +
// '<span class="nav-label"></span>Reported Accounts</a>' +
// '</li>' +
// '<li>'+ '<a href="reported_feedbacks.html" class=" hvr-bounce-to-right">' + '<i class="fa fa-list-ol nav_icon"></i>' +
// '<span class="nav-label">Reported Feedbacks</span> </a>' +
// '</li>' +
// '<li> ' + '<a href="verified_account.html" class=" hvr-bounce-to-right">' + '<i class="fa fa-users nav_icon"></i>' +
// '<span class="nav-label">Hotel Applicant</span>' + '</a>' + '</li>'
// }
