var hostname = document.location.host;

angular.module('myApp').factory('AuthService',
    ['$q', '$timeout', '$http', '$window',
        function ($q, $timeout, $http, $window) {



           function login_hotel(email, password) {

                // create a new instance of deferred
                var deferred = $q.defer();

                // send a post request to the server
                $http.post('http://127.0.0.1:5000/api/v1.0/login/', {email: email, password: password})
                    // handle success
                    .success(function (data, status, headers, config) {
                        console.log("ok");
                        if (status === 200 && data['status']) {
                            $window.sessionStorage.token = data["token"];
                            $window.sessionStorage.setItem('hotel_id', data["hotel_id"]);
                            $window.sessionStorage.setItem('type', data["login_type"]);
                            deferred.resolve();
                        } else {
                            deferred.reject();
                        }
                    })
                    // handle error
                    .error(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;

            }

            function login_personnel(email, password) {

                // create a new instance of deferred
                var deferred = $q.defer();

                // send a post request to the server
                $http.post('http://127.0.0.1:5000/api/v1.0/personnel/login/', {email: email, password: password})
                    // handle success
                    .success(function (data, status, headers, config) {
                        console.log("ok");
                        if (status === 200) {
                            $window.sessionStorage.token = data["token"];
                            $window.sessionStorage.setItem('hotel_id', data["hotel_id"]);
                            $window.sessionStorage.setItem('type', data["login_type"]);
                            deferred.resolve();
                        } else {
                            deferred.reject();
                        }
                    })
                    // handle error
                    .error(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;

            }

            function login_admin(email, password) {

                // create a new instance of deferred
                var deferred = $q.defer();

                // send a post request to the server
                $http.post('http://127.0.0.1:5000/api/v1.0/login/', {email: email, password: password})
                    // handle success
                    .success(function (data, status, headers, config) {
                        console.log("ok");
                        if (status === 200) {
                            $window.sessionStorage.token = data["token"];
                            $window.sessionStorage.setItem('hotel_id', data["hotel_id"]);
                            $window.sessionStorage.setItem('type', data["login_type"]);
                            deferred.resolve();
                        } else {
                            deferred.reject();
                        }
                    })
                    // handle error
                    .error(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;

            }


            function logout() {

                // create a new instance of deferred
                var deferred = $q.defer();

                // send a get request to the server
                $http.get('http://127.0.0.1:5000/api/v1.0/hotel_personnel/logout')
                    // handle success
                    .success(function (data) {
                        deferred.resolve();
                    })
                    // handle error
                    .error(function (data) {
                        user = false;
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;

            }

        
            function registerHotelpersonnel(password, fname, mname, lname) {

                // create a new instance of deferred
                var deferred = $q.defer();

                // send a post request to the server
                $http.post('http://127.0.0.1:5000/api/v1.0/hotel_personnel/register', {password: password, fname: fname, mname: mname, lname: lname})
                    // handle success
                    .success(function (data, status) {
                        if (status === 200 && data.result) {
                            user = true;
                            deferred.resolve();
                        } else {
                            user = false;
                            deferred.reject();
                        }
                    })
                    // handle error
                    .error(function (data) {
                        user = false;
                        deferred.reject();
                    });


                // return promise object
                return deferred.promise;

            }

             function registerHotel(email, hotel_name, url, description, paypal_account, password, address, contact, rooms) {

                // create a new instance of deferred
                var deferred = $q.defer();

                // send a post request to the server
                $http.post('http://127.0.0.1:5000/api/v1.0/hotels/signup/', {email: email, hotel_name: hotel_name, url: url, 
                        description: description, paypal_account: paypal_account, password: password, address: address, 
                        contact: contact, rooms: rooms})
                    // handle success
                    .success(function (data, status) {
                        if (status === 200) {
                            deferred.resolve();
                        } else {
                            deferred.reject();
                        }
                    })
                    // handle error
                    .error(function (data) {
                        deferred.reject();
                    });


                // return promise object
                return deferred.promise;

            }


            // return available functions for use in controllers
            return ({
                login_hotel: login_hotel,
                login_personnel: login_personnel,
                login_admin: login_admin,
                logout: logout,
                registerHotelpersonnel: registerHotelpersonnel,
                registerHotel: registerHotel
            });

        }


    ]);
angular.module('myApp').factory('searchService', [
    '$q', '$timeout', '$http',
        function ($q, $timeout, $http) {
            function search(location, hotel_name, roomtype, price) {

                // create a new instance of deferred
                var deferred = $q.defer();
                // send a post request to the server
                $http.post('http://127.0.0.1:5000/api/v1.0/search/', {'location': location})
                    // handle success
                    .success(function (data) {
                        deferred.resolve(data);
                    })
                    // handle error
                    .error(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;
                // public API

            }

            function hotelId(id) {

                // create a new instance of deferred
                var deferred = $q.defer();
                // send a post request to the server
                $http.get('http://127.0.0.1:5000/api/v1.0/hotels/' + id + '/')
                    // handle success
                    .success(function (data) {
                        deferred.resolve(data);
                    })
                    // handle error
                    .error(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;
                // public API

            }

            function getFeaturesList(id) {

                // create a new instance of deferred
                var deferred = $q.defer();
                // send a post request to the server
                $http.get('http://127.0.0.1:5000/api/v1.0/featureslist/' + id + '/')
                    // handle success
                    .success(function (data) {
                        deferred.resolve(data);
                    })
                    // handle error
                    .error(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;
                // public API

            }            

            function create_transaction(checkin, checkout, cost, hotel_id, room_id) {

                // create a new instance of deferred
                var deferred = $q.defer();
                // send a post request to the server
                $http.post('http://127.0.0.1:5000/api/v1.0/hotel/transaction/', {checkin: checkin, checkout: checkout,
                                                                              cost: cost, hotel_id: hotel_id, room_id: room_id})
                    // handle success
                    .success(function (data) {
                        deferred.resolve(data);
                    })
                    // handle error
                    .error(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;
                // public API

            }


            return ({
                search: search,
                hotelId: hotelId,
                getFeaturesList: getFeaturesList,
                create_transaction: create_transaction
            });
        }
]);
angular.module('myApp').factory('hotelService', [
    '$q', '$timeout', '$http',
    function ($q, $timeout, $http) {

        function hotelpersonnel() {

            // create a new instance of deferred
            var deferred = $q.defer();
            // send a post request to the server
            var data = sessionStorage.getItem('hotel_id');
            $http.get('http://127.0.0.1:5000/api/v1.0/personnels/' +  data + '/')
                // handle success
                .success(function (data) {
                    deferred.resolve(data);
                })
                // handle error
                .error(function (data) {
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;
            // public API

        }

        function addhotelpersonnel(fname, mname, lname, email, password) {

            // create a new instance of deferred
            var deferred = $q.defer();
            // send a post request to the server
            var data = sessionStorage.getItem('hotel_id');
            $http.post('http://127.0.0.1:5000/api/v1.0/hotelpersonnel/',
                {
                    'fname': fname,
                    'mname': mname,
                    'lname': lname,
                    'email': email,
                    'personnel_password': password,
                    'hotel_id': data
                })
                // handle success
                .success(function (data) {
                    deferred.resolve(data);
                })
                // handle error
                .error(function (data) {
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;
            // public API

        }


        function hotel_viewroom() {

            // create a new instance of deferred
            var deferred = $q.defer();
            // send a post request to the server
            var data = sessionStorage.getItem('room_id');
            $http.get('http://127.0.0.1:5000/api/v1.0/room/' + data + '/')
                // handle success
                .success(function (data) {
                    deferred.resolve(data);
                })
                // handle error
                .error(function (data) {
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;
            // public API
        }

        function addhotelroom(room_type, cost, max_person, total_room, hotel_id) {

            // create a new instance of deferred
            var deferred = $q.defer();
            // send a post request to the server
            var data = sessionStorage.getItem('room_id');
            $http.post('http://127.0.0.1:5000/api/v1.0/room/',
                {
                    'room_type': room_type,
                    'cost': cost,
                    'max_person': max_person,
                    'total_room': total_room,
                    'hotel_id': hotel_id,
                    'id': data
                })
                // handle success
                .success(function (data) {
                    deferred.resolve(data);
                })
                // handle error
                .error(function (data) {
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;
            // public API

        }

        return ({
            hotelpersonnel: hotelpersonnel,
            addhotelpersonnel: addhotelpersonnel,
            hotel_viewroom: hotel_viewroom,
            addhotelroom: addhotelroom
        });
    }]);


angular.module('myApp').factory('personnelService', [
    '$q', '$timeout', '$http',
        function ($q, $timeout, $http) {

            function gethoteltransaction() {

                // create a new instance of deferred
                var deferred = $q.defer();
                // send a post request to the server
                var data = sessionStorage.getItem('hotel_id');
                $http.get('http://127.0.0.1:5000/api/v1.0/hotelpersonnel/' +  data + '/transaction/view' )
                    // handle success
                    .success(function (data) {
                        deferred.resolve(data);
                    })
                    // handle error
                    .error(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;
                // public API

            }

            function confirmTransaction(transaction_number, fee, payment_id, payer_id) {

                // create a new instance of deferred
                var deferred = $q.defer();
                // send a post request to the server
                var data = sessionStorage.getItem('hotel_id');
                $http.post('http://127.0.0.1:5000/api/v1.0/hotel/transaction/confirm', { transaction_number: transaction_number,
                                                                        fee: fee, payment_id: payment_id, payer_id: payer_id, id: data} )
                    // handle success
                    .success(function (data) {
                        deferred.resolve(data);
                    })
                    // handle error
                    .error(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;
                // public API

            }


            return ({
                gethoteltransaction: gethoteltransaction,
                confirmTransaction: confirmTransaction

            });
        }
]);

angular.module('myApp').factory('transactionService',
    ['$q', '$timeout', '$http',
        function ($q, $timeout, $http) {



            function afterpayment(payment_id, payer_id, token, name, email, contact_number ) {

                // create a new instance of deferred
                var deferred = $q.defer();

                // send a post request to the server
                $http.put('http://127.0.0.1:5000/api/v1.0/hotel/transaction/execute', {'payment_id': payment_id, 'payer_id': payer_id, 'token': token, 'name': name,
                                                                                        'email': email, 'contact_number': contact_number})
                    // handle success
                    .success(function (data, status, headers, config) {

                            deferred.resolve();

                    })
                    // handle error
                    .error(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;

            }


            // return available functions for use in controllers
            return ({
                afterpayment: afterpayment
            });

        }


    ]);


angular.module('myApp').factory('authInterceptor', function ($rootScope, $q, $window) {
  return {
    request: function (config) {
      config.headers = config.headers || {};
      if ($window.sessionStorage.token) {
        config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
      }
      return config;
    },
    response: function (response) {
      if (response.status === 401) {
        // handle the case where the user is not authenticated
      }
      return response || $q.when(response);
    }
  };
});

angular.module('myApp').config(function ($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
});




angular.module('myApp').factory('systemAdminService', [
    '$q', '$timeout', '$http',
        function ($q, $timeout, $http) {
            function accounts() {

                // create a new instance of deferred
                var deferred = $q.defer();
                // send a post request to the server
                $http.get('http://127.0.0.1:5000/api/v1.0/business_infos/')
                    // handle success
                    .success(function (data) {
                        deferred.resolve(data);
                    })
                    // handle error
                    .error(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;
                // public API

            }

            function verify(id) {

                // create a new instance of deferred
                var deferred = $q.defer();
                // send a post request to the server
                $http.put('http://127.0.0.1:5000/api/v1.0/verify_hotel/', {'id': id})
                    // handle success
                    .success(function (data) {
                        deferred.resolve(data);
                    })
                    // handle error
                    .error(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;
                // public API

            }
            return ({
                accounts : accounts,
                verify : verify
            });

 }]);


angular.module('myApp').factory('imageService',
    ['$q', '$timeout', '$http',
        function ($q, $timeout, $http) {

        function upload(img) {
                var deferred = $q.defer();
                var data = sessionStorage.getItem('hotel_id');
                $http.post('http://127.0.0.1:5000/api/v1.0/upload/image/', img)
                    // handle success
                    .success(function (data) {
                        deferred.resolve(data);
                    })
                    // handle errorsss
                    .error(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;
                // public API

            }

        function getImage() {

            var deferred = $q.defer();
            var data = sessionStorage.getItem('hotel_id');
            $http.get('http://127.0.0.1:5000/api/v1.0/image/' + data + '/')
            .success(function (data) {
                deferred.resolve(data);
            })
                    // handle errorsss
            .error(function (data) {
                deferred.reject();
            });

                // return promise object
            return deferred.promise;
                // public API
            }

            // return available functions for use in controllers
            return ({
                upload: upload,
                getImage: getImage
            });

        
    }
    ]);

