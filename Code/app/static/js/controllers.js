angular.module('myApp').controller('userController',
    ['$scope', '$location', '$window','AuthService',
        function ($scope, $location ,$window, AuthService) {
            $scope.isAuthenticated = false;
            $scope.loginhotel = function () {

                // initial values
                $scope.error = false;
                $scope.disabled = true;

             $scope.disabled = true;
                 AuthService.login_hotel($scope.loginForm.email, $scope.loginForm.password)
                // handle success
                    .then(function () {
                      //$location.path('/hoteladmin');
                        $window.location.href = "http://site-teamireserve.rhcloud.com/index2.html#/hoteladmin"
                        $scope.isAuthenticated = true;
                        $scope.disabled = false;
                        $scope.loginForm = {};
                    })
                    // handle error
                    .catch(function () {
                        delete $window.sessionStorage.token;
                        $scope.isAuthenticated = false;
                        $scope.error = true;
                        $scope.errorMessage = "Invalid username and/or password";
                        $scope.disabled = false;
                        $scope.loginForm = {};
                    });
            };
                    $scope.loginpersonnel = function () {

                        // initial values
                        $scope.error = false;
                        $scope.disabled = true;

                        // call login from service
                        AuthService.login_personnel($scope.loginForm.email, $scope.loginForm.password)
                        // handle success
                            .then(function (res) {
                                    $window.location.href = "http://site-teamireserve.rhcloud.com/index3.html#/hotelpersonnel"
                                    $scope.isAuthenticated = true;
                                    $scope.disabled = false;
                                    $scope.loginForm = {};
                            })
                            // handle error
                            .catch(function () {
                                delete $window.sessionStorage.token;
                                $scope.isAuthenticated = false;
                                $scope.error = true;
                                $scope.errorMessage = "Invalid username and/or password";
                                $scope.disabled = false;
                                $scope.loginForm = {};
                            });
                    };
                            $scope.loginadmin = function () {

                                // initial values
                                $scope.error = false;
                                $scope.disabled = true;

                                // call login from service
                                AuthService.login_admin($scope.loginForm.email, $scope.loginForm.password)
                                    // handle success
                                    .then(function (res) {
                                        $window.location.href = "http://site-teamireserve.rhcloud.com/index4.html#/admin"
                                        $scope.isAuthenticated = true;
                                        $scope.loginForm = {};
                                    })
                                    // handle error
                                    .catch(function () {
                                        delete $window.sessionStorage.token;
                                        $scope.isAuthenticated = false;
                                        $scope.error = true;
                                        $scope.errorMessage = "Invalid username and/or password";
                                        $scope.disabled = false;
                                        $scope.loginForm = {};
                                    });

            };

             $scope.logout = function () {
                 $scope.isAuthenticated = false;
                 delete $window.sessionStorage.token;
                 delete $window.sessionStorage.hotel_id;
                 $window.location.href = "index.html";
             };
             $scope.register = function () {

                // initial values
                $scope.error = false;
                $scope.disabled = true;

                // call register from service
                AuthService.registerHotel(
                    $scope.registerForm.email,
                    $scope.registerForm.hotel_name,
                    $scope.registerForm.url,
                    $scope.registerForm.description,
                    $scope.registerForm.paypal,
                    $scope.registerForm.password,
                    $scope.registerForm.address,
                    $scope.registerForm.contact,
                    $scope.registerForm.room)

                    // handle success
                    .then(function () {
                        $location.path('/login');
                        $scope.disabled = false;
                        $scope.registerForm = {};
                    })
                    // handle error
                    .catch(function () {
                        $scope.error = true;
                        $scope.errorMessage = "Something went wrong!";
                        $scope.disabled = false;
                        $scope.registerForm = {};
                    });

            };


        }]);
glob_results = [];
glob_hotel = [];
angular.module('myApp').controller('searchController',
    ['$scope', '$location', '$window', 'searchService',
        function ($scope, $location,$window, searchService) {
            $scope.results = glob_results;
                $scope.search = function () {
                    searchService.search($scope.searchForm.location)
                        // handle success
                        .then(function (resp) {
                            $location.path('/search');
                            glob_results = resp["entries"];
                            console.log(resp["entries"]);

                        })
                        // handle error
                        .catch(function () {

                        });
                };
                $scope.hotel_id = glob_hotel;
                $scope.hotel = function (id) {
                    searchService.hotelId(id)
                    // handle success
                        .then(function (resp) {
                            $location.path('/profile');
                            glob_hotel = resp;
                            $scope.hotel_id = glob_hotel;
                            console.log(resp);

                        })
                        // handle error
                        .catch(function () {

                        })
                };


                $scope.features_list = function(id) {
                    searchService.getFeaturesList(id)
                        .then(function (resp) {
                            $scope.list = resp['entries'];
                            console.log(resp['entries']);
                        })
                        .catch(function () {

                        })
                };

                $scope.init_transaction = function(checkin, checkout, cost, hotel_id, room_id) {
                    searchService.create_transaction(checkin, checkout, cost, hotel_id, room_id)
                      .then(function (resp) {
                        var redirectWindow = $window.open(resp['redirect'], '_blank');
                        redirectWindow.location;
                      })
                      .catch(function () {

                      })
                };

        }]);
glob_hotelpersonnel = [];
glob_hotelroom = [];
angular.module('myApp').controller('hotelAdminController',
    ['$scope', '$location', 'hotelService',
        function ($scope, $location, hotelService) {

          $scope.personnel_hotel = glob_hotelpersonnel;
                $scope.hotel_admin_viewhotelpersonnel = function () {
                    hotelService.hotelpersonnel()
                        // handle success
                        .then(function (res) {
                            $location.path('/hoteladmin/viewpersonnel');
                            glob_hotelpersonnel = res['entries'];
                            $scope.personnel_hotel = glob_hotelpersonnel;
                            console.log(res['entries']);
                        })
                        // handle error
                        .catch(function () {

                        });
                };

                $scope.hotel_admin_addpersonnel = function (){
                  hotelService.addhotelpersonnel($scope.registerForm.fname, $scope.registerForm.mname, $scope.registerForm.lname,
                                                  $scope.registerForm.email, $scope.registerForm.personnel_password)
                    .then(function (res) {
                      $location.path('/hoteladmin/viewpersonnel');
                      $scope.message = "Successfully Registered";
                      console.log(res);
                    })

                    .catch(function (res) {
                      $scope.message = "Invalid Input";
                      console.log(res);
                    });
                };

                //add room

            $scope.room_hotel = glob_hotelroom;
                $scope.hotel_admin_view_room = function () {
                    hotelService.hotel_viewroom
                        .then(function (res) {
                            $location.path('/hoteladmin/viewrooms');
                            glob_hotelroom = res['entries'];
                            $scope.room_hotel = glob_hotelroom;
                            console.log(res['entries']);
                        })
                        // handle error
                        .catch(function () {

                        });
                };

                $scope.hotel_admin_add_room = function (){
                  hotelService.addhotelroom($scope.addForm.room_type, $scope.addForm.cost, $scope.addForm.max_person,
                                            $scope.addForm.total_room, $scope.addForm.hotel_id)
                    .then(function (res) {
                      $location.path('/hoteladmin/viewrooms');
                      $scope.message = "Successfully Added";
                      console.log(res);
                    })

                    .catch(function (res) {
                      $scope.message = "Invalid Input";
                      console.log(res);
                    });
                };


        }]);


glob_hoteltransaction = [];
angular.module('myApp').controller('hotelPersonnelController',
    ['$scope', '$location', 'personnelService',
        function ($scope, $location, personnelService) {

          $scope.transaction = glob_hoteltransaction;
                $scope.get_transaction = function () {
                    personnelService.gethoteltransaction()
                        // handle success
                        .then(function (resp) {
                            $location.path('/hotelpersonnel/transaction');
                            glob_hoteltransaction = resp['entries'];
                            $scope.transaction = glob_hoteltransaction;
                            console.log(resp['entries']);
                        })
                        // handle error
                        .catch(function () {

                        });
                };

                $scope.confirm_transaction = function (transaction_number, fee, payment_id, payer_id) {
                    personnelService.confirmTransaction(transaction_number, fee, payment_id, payer_id)
                    .then(function (resp) {
                            $location.path('/hotelpersonnel/transaction');
                        })
                        // handle error
                    .catch(function () {

                    });
                }

        }]);


angular.module('myApp').controller('transactionController',
    ['$scope', '$location', '$window','transactionService',
        function ($scope, $location, $window, transactionService) {

                $scope.loading = true;
                $scope.afterpayment = function () {
                    transactionService.afterpayment($location.search().paymentId,$location.search().PayerID, 
                                        $location.search().token, $scope.additionalForm.name, $scope.additionalForm.email,
                                        $scope.additionalForm.contactnumber)
                        .then(function (resp) {
                            $location.path('');
                        })
                        .catch(function () {

                        });
                };

             
        }]);



angular.module('myApp').controller('imageController', ['$scope', '$http', 'imageService',
function($scope, $http, imageService){
    

    $scope.filesChanged = function(elm) {
        $scope.files=elm.files
        $scope.$apply();
    }

    $scope.uploadImage = function() {
     var f = document.getElementById('img_file').files[0]; console.log(f);
     var formData = new FormData();
     formData.append('file', f);
                        $http({method: 'POST', url: 'http://siteapi-teamireserve.rhcloud.com/api/v1.0/upload/image/',
                         data: formData,
                         headers: {'Content-Type': 'multipart/form-data'},
                         transformRequest: angular.identity})
                        .success(function(data, status, headers, config) {console.log(data);
                        if (data.success) {
                            console.log('import success!');

                        }
                    })
                    .error(function(data, status, headers, config) {
                    });
            // }
        };

    $scope.getImage = function() {
        imageService.getImage()
        .then(function(resp) {
            console.log(resp);
            $scope.images = resp['img'];
        })
        .catch(function (resp) {

        });
    }
    
    }]);



glob_verif = [];
angular.module('myApp').controller('systemAdminController',
  ['$scope', '$location', '$window', 'systemAdminService',
      function ($scope, $location,$window, systemAdminService) {
          $scope.acct = glob_verif;
          $scope.view_accounts = function () {
          systemAdminService.accounts()
                                // handle success
            .then(function (resp) {
              glob_verif = resp["entries"];
              $scope.acct = glob_verif;
              console.log(resp["entries"]);

          })
                                // handle error
           .catch(function () {

        });

      };

      $scope.verify_account = function (id) {
      systemAdminService.verify(id)
                            // handle success

        .then(function (resp) {
          console.log(resp["message"]);

      })
                            // handle error
       .catch(function () {

    });

  };

}]);
